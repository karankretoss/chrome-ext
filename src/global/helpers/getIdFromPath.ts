export default function getIdFromPath(pathname: string): string {
  const strArray = pathname.split("/");
  if (strArray.length === 3) {
    return strArray[2];
  }
  return pathname;
}
