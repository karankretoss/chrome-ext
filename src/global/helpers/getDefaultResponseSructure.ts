export interface IResponse {
  successful: boolean;
  messages: { code: string; message: string }[];
  data: { [key: string]: any } | { [key: string]: any }[];
}

export default function getDefaultResponseSructure(): IResponse {
  return {
    successful: true,
    messages: [] as { code: string; message: string }[],
    data: {},
  };
}
