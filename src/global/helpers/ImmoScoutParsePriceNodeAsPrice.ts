import Measurement from "../entities/reusable/types/Price";
import getNumberFromPrice from "./getNumberFromPrice";

export default function ImmoScoutParsePriceNodeAsPrice(
  priceNode: HTMLDivElement,
  priceCurrencySeparator: string
): Measurement | undefined {
  try {
    const priceText = (priceNode.querySelector(
        "dd:first-child"
    ) as HTMLDivElement).innerText;
    const price = ImmoScoutParsePriceAsPrice(priceText, priceCurrencySeparator);
    const labelNode = priceNode.querySelector("dt") as HTMLDivElement;
    const label = !!labelNode ? labelNode.innerText.trim() : "";

    if (!price) {
      return undefined;
    }
    return {
      ...price,
      label,
    };
  } catch (_) {return undefined}
}

export function ImmoScoutParsePriceGroupAsPrice(
  priceNode: HTMLDivElement,
  priceCurrencySeparator: string
): Measurement | undefined {
  try {
    const price = ImmoScoutParsePriceAsPrice(priceNode.innerText, priceCurrencySeparator);
    const label = 'Kaufpreis';

    if (!price) {
      return undefined;
    }
    return {
      ...price,
      label,
    };
  } catch (_) {return undefined}
}

export function ImmoScoutParsePriceAsPrice(
  price: string,
  priceCurrencySeparator: string
): Measurement | undefined {
  try {
    const firstLine = price.split("\n")[0].trim();
    if (firstLine === "Preis auf Anfrage") {
      return undefined;
    }
    const priceArr = firstLine.split(priceCurrencySeparator);
    if (priceArr.length !== 2) {
      return undefined;
    }
    return {
      number: getNumberFromPrice(priceArr[0]),
      measure: priceArr[1],
      label: "",
    };
  } catch (_) {return undefined}

}

export function WillHabenParsePriceAsPrice(
  price: string,
  priceCurrencySeparator: string
): Measurement | undefined {
  try {
    let label = '';
    const priceArr = price.split(priceCurrencySeparator);

    if (priceArr.length !== 2) {
      if (priceArr.length === 3 && priceArr[0] === 'ab') {
        label = 'Kaufpreis ab';
        priceArr.shift();
      } else {
        return undefined;
      }
    }
    return {
      number: getNumberFromPrice(priceArr[1]),
      measure: priceArr[0],
      label,
    };
  } catch (_) {return undefined}
}

export function ImmoscoutGetParseMeasurement(
  measurementNode: HTMLElement | string | undefined,
  measurementSeparator?: string,
): Measurement | undefined {
  if (!measurementNode) {
    return;
  }
  let rawStr: string;
  if ('string' === typeof measurementNode) {
    rawStr = measurementNode;
  } else if (measurementNode.innerText && 'string' === typeof measurementNode.innerText) {
    rawStr = measurementNode.innerText;
  } else {
    return;
  }
  return stringToMeasurement(rawStr.replace(/\s+/g, ' ').trim(), ' ');
}

function stringToMeasurement(
  str: string,
  measurementSeparator?: string,
): Measurement | undefined {
  // Assume measurement str must start with number value
  const measureArr = !!measurementSeparator ? str.split(measurementSeparator) : [ str, '' ];
  if ('+' === measureArr[0] || '-' === measureArr[0]) {
    const posMark = measureArr.shift();
    measureArr[0] = posMark + measureArr[0];
  }
  if (!measureArr[0].match(/^\d/)) {
    measureArr.reverse();
  }
  const [ value, measure, ...rest ] = measureArr;
  const label = rest.join(' ');
  const number = getNumberFromPrice(value) || 0;

  return {
    number,
    measure: !!measure ? measure : '',
    label: !!number ? label : str,
  };
}
