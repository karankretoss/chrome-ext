export const featuresMap: Record<string, string> = {
    "Online-Besichtigung": "onlineViewing",
    "Personenaufzug": "passengerElevator",
    "Balkon/ Terrasse": "balconyTerrace",
    "Keller": "basement",
    "Stufenloser Zugang": "steplessAccess",
    "Provisionsfrei": "commissionFree",
    "Garten/ -mitbenutzung": "sharedGarden",
    "Garten/-mitbenutzung": "sharedGarden",
    "Gartenanlage/ Park": "gardens_park",
    "Gäste-WC": "guestToilet",
    "Einbauküche": "builtinKitchen",
    "Als Ferienwohnung geeignet": "suitableForVacation",
    "Als Ferienhaus geeignet": "suitableForVacation",
    "Vermietet": "rented",
    "Einliegerwohnung": "housingUnit",
    "Gästeappartements": "housingUnit",
    "Küche": "kitchen",
    "Eigene Kochmöglichkeit": "ownCookingFacilities",
    "Hauseigene Küche": "inhouseKitchen",
    "Starkstrom": "heavyCurrent",
    "Barrierefrei": "accessible",
    "Barrierefreiheit": "accessible",
    "Behindertengerecht": "accessible",
    "Kantine/Cafeteria": "cafeteria",
    "Restaurant/ Cafe": "cafeteria",
    "Provisionsfrei für Kaufende": "noCommissionForBuyer",
    "Rampe": "ramp",
    "Kranbahn": "craneRunway",
    "Lastenaufzug": "freightElevator",
    "Hebebühne": "rampLift",
    "Gastterrasse": "guestTerrace",
    "Kurzfristig bebaubar": "shortNoticeBuildable",
    "Baugenehmigung": "buildingPermit",
    "Abriss": "demolition",
    "Zweitversteigerung": "secondAuction",
    "WG-geeignet": "flatShareSuitable",
    "Wohnberechtigungsschein erforderlich": "eligibilityCertificateRequired",
    "Öffentl. Verkehrsmittel erreichbar": "publicTransportAccessible",
    "Garage/ Stellplatz": "garage_parkingSpace"
};