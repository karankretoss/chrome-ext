export default function getNumberFromPrice(price: string): number {
  let isDecimalDot: boolean = true;
  const commaPosition: number = price.indexOf(",");
  const dotPosition: number = price.indexOf(".");
  const hasComma: boolean = -1 < commaPosition;
  const hasDot: boolean = -1 < dotPosition;
  if (hasComma && hasDot) {
    isDecimalDot = commaPosition < dotPosition;
  } else if (hasComma) {
    isDecimalDot = isDecimalDotted(price, ",");
  } else if (hasDot) {
    isDecimalDot = isDecimalDotted(price, ".");
  } else {
    try {
      return parseInt(price);
    } catch (error) {
      return 0;
    }
  }

  try {
    if (isDecimalDot) {
      return parseFloat(price.replace(/,/gm, ""));
    } else {
      return parseFloat(price.replace(/\./gm, "").replace(",", "."));
    }
  } catch (e: any) {
    throw new Error(
      `Failed to convert '${price}' to float due to: ${e.message}.`
    );
  }
}

function isDecimalDotted(price: string, splitter: string): boolean {
  const priceParts: string[] = price.split(splitter);
  const isThousandsSplitter =
    2 < priceParts.length || priceParts[priceParts.length - 1].length === 3;
  return (
    ("." === splitter && !isThousandsSplitter) ||
    ("." !== splitter && isThousandsSplitter)
  );
}
