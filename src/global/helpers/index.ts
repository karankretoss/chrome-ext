const transformValue = (val: any) => {
    if (typeof val === 'string') {
        return val.trim();
    }
    if (val && val.number) {
        return val.number;
    }
    return JSON.stringify(val).trim()
}

export const isValueOfEntitiesEqual = (e1: any, e2: any) => {
    if (e1 && e2) {
        try {
            return (transformValue(e1) === transformValue(e2));
        } catch (_) {}
    }

    return true;
}
