export const countryMap: Record<string, string| undefined> = {
    '': undefined,
    '276': 'DEU',
    '724': 'ESP',
    '40': 'AUT',
};
