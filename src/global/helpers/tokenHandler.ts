export async function getToken(): Promise<any> {
    return new Promise((resolve) => {
        chrome.storage.sync.get("token", (result) => {
            if (result && result.token) {
                return resolve(result.token);
            }
            resolve(undefined);
        });
    });
}

export async function setToken(token: string): Promise<void> {
    return new Promise((resolve) => {
        chrome.storage.sync.set({ token }, () => {
            resolve();
        });
    });
}
