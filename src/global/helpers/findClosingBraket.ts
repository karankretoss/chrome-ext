export function findClosingBracket(str: string, openIndex: number): number | undefined {
    const stack = [];
    for (let i = openIndex + 1; i < str.length; i++) {
        if (str[i] === '[') {
            stack.push(str[i]);
        } else if (str[i] === ']') {
            stack.pop();
            if (stack.length === 0) {
                return i;
            }
        }
    }
    return undefined; // no closing bracket found
}
