export const waitForPageReady = (readinessConditionFunction?: () => Boolean) =>
    new Promise((resolve, _reject) => {
        let retries = 7;
        let interval = setInterval(() => {
            if (readinessConditionFunction!() || retries-- < 1) {
                clearInterval(interval);
                resolve(true);
            }
        }, 1000);
    });