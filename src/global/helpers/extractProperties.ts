import Measurement from "../entities/reusable/types/Price";
import {
    FullPageAdditionalProperties, ScrapedEntityWithAdditionalProperties,
    FullPageScrapeSelector,
    PropertySelectorAndTextParser
} from "../entities/reusable/types/ScrapedEntity";
import {PropertyAddress} from "../entities/reusable/types/PropertyAddress";
import { countryMap } from "./countries.map";

const removeChild = (node: HTMLElement, selector?: string): void => {
    if (selector !== undefined) {
        const childNode = node.querySelector(selector) as HTMLElement;
        if (childNode) {
            node.removeChild(childNode);
        }
    }
};

export const extractProperty = (entityDiv: HTMLElement, propertySelectors: PropertySelectorAndTextParser[]): string | Measurement | undefined => {
    let value: string | Measurement | undefined = undefined;
    for (let selector of propertySelectors) {
        if (value !== undefined)
            break;
        try {
            if (selector.matchText === undefined) {
                const propertyNode = entityDiv.querySelector(selector.selector) as HTMLElement;
                removeChild(propertyNode, selector.removeChildSelector);
                if (!propertyNode)
                    continue;
                if (selector.textParser) {
                    value = selector.textParser(propertyNode);
                } if (selector.measureParser) {
                    value = selector.measureParser(propertyNode);
                } else {
                    value = (propertyNode).innerText;
                }
            } else {
                const propertyNodes = entityDiv.querySelectorAll(selector.selector) as NodeListOf<HTMLElement>;
                for (let node of Array.from(propertyNodes)) {
                    const node_ = node;
                    if (node_.innerText?.includes(selector.matchText)) {
                        value = node_.innerText?.replace(selector.matchText, '');
                        if (selector.measureParser) {
                            value = selector.measureParser(value);
                        } 
                    }
                }
            }
        } catch (_) {
        }
    }

    // Trimming special character
    if (value && 'string' === typeof value) {
        value = value.trim().replace(/^\n+|\n+$/g, '');
    } else if (value && 'object' === typeof value && 'string' === typeof value.measure) {
        value.measure = value.measure.trim().replace(/^\n+|\n+$/g, '');
        value.label = value.label.trim().replace(/^\n+|\n+$/g, '');
    }

    return value;
}

export const extractProperties = (entityDiv: HTMLElement, extraPropertiesSelector: FullPageScrapeSelector): ScrapedEntityWithAdditionalProperties => {
    let result: any = {}

    for (let property in extraPropertiesSelector) {
        const pr = property as FullPageAdditionalProperties;
        const propertySelectors = extraPropertiesSelector[pr] as PropertySelectorAndTextParser[];
        result[pr] = extractProperty(entityDiv, propertySelectors);
    }

    return result;
}

const getCountryDataFromExposePage = () => {
    try {
        return countryMap[window?.adsData?.expose?.locationAddress?.qualifiedGeoIds?.countryId];
    } catch (error: any) {
        return undefined;
    }
}

export const parsePropertyAddressFromAddressInExposedPage = (address: string | undefined): PropertyAddress | undefined => {
    // Invalid address
    if (!address)
        return undefined

    const propertyAddress: PropertyAddress = {}
    const parts = address.split(',')
        .map(part => part.replace('\n', ''))
        .map(part => part.trim())

    let cityIndex: number | undefined = undefined
    let streetIndex: number | undefined = undefined


    parts.forEach((part, index) => {
        // Find Zip code & city. E.g. 12334 Berlin
        const zipCodeAndCityRegrex = /^(\d{4,5})\s+(.+)/;
        const matchZipCodeAndCity = part.match(zipCodeAndCityRegrex);

        if (matchZipCodeAndCity) {
            propertyAddress.zipCode = matchZipCodeAndCity[1]
            propertyAddress.city = matchZipCodeAndCity[2]
            cityIndex = index
        }

        // Find street and house number
        const streetAndHouseNumberRegrex =  /^(.+\b)\s+(\d+.*)$/;
        const matchStreetAndHouseNumberRegrex = part.match(streetAndHouseNumberRegrex);

        if (matchStreetAndHouseNumberRegrex) {
            propertyAddress.street = matchStreetAndHouseNumberRegrex[1]
            propertyAddress.houseNumber = matchStreetAndHouseNumberRegrex[2]
            streetIndex = index
        }
    })

    if (cityIndex === undefined) {
        if (parts.length && parts.length - 1 !== streetIndex) {
            propertyAddress.city = parts[parts.length - 1]
        }
    } else {
        if (cityIndex - 1 !== streetIndex) {
            propertyAddress.quarter = parts[cityIndex - 1]
        }
    }

    propertyAddress.country ??= getCountryDataFromExposePage();

    return propertyAddress
}