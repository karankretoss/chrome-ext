export const SYNC_ENTITIES_DATA_WITH_SERVER =
  "SYNC_ENTITIES_DATA_WITH_SERVER";
// export const BACKEND_SERVER_URL = "http://localhost:3909";
export const BACKEND_SERVER_URL = 'http://databe01.secseek.com:3009';
export const REALESTATE_BE_EXT_ENDPOINT = `${BACKEND_SERVER_URL}/api/v1/realestate/extention`;
export const MAX_RETRY_GET_COUNTRY_AT_EXPOSE_PAGE = 50
export const INTERVAL_GET_COUNTRY_AT_EXPOSE_PAGE = 300 // millisecond
export const GLOBAL_CHECK_PERMS = "GLOBAL_CHECK_PERMS";
export enum className {
    Indicator='rotel-kapsel-indicator',
    IndicatorDot='rote-kapsel-indicator__dot'
}