import domReady from "./domReady";

export default async function addStyleString(
  str: string,
  specificNode?: HTMLElement
) {
  const node = document.createElement("style");
  await domReady();

  node.innerHTML = str;
  if (specificNode) {
    specificNode.appendChild(node);
  } else {
    const allDivs = document.getElementsByTagName("div");
    allDivs[Math.ceil(Math.random() * allDivs.length)].appendChild(node);
  }
}
