import Popper, { PopperOptions } from 'popper.js';
import addStyleString from './addStyleString';

const POPPER_CLASS_NAME = 'popper';

const createTooltipElementContainerInDOM = (html?: string) => {
  const tooltipEl = document.createElement('div');
  tooltipEl.className = `${POPPER_CLASS_NAME} hide`;
  tooltipEl.innerHTML = `
  ${html}
  <div class="${POPPER_CLASS_NAME}__arrow"></div>
  `;
  document.body.appendChild(tooltipEl);
  return tooltipEl;
};

function popover(
  destination: HTMLElement,
  props: {
    placement: string;
    html: string;
    afterRender?: (popupElement: HTMLElement, data: any) => void;
  },
  data: any
): void {
  let tooltipEl: HTMLElement;
  if ((<any>destination)._extPopup) {
    destination.removeEventListener(
      'click',
      (<any>destination)._extPopup.mouseEnterHandler
    );
  }

  (<any>destination)._extPopup = {
    mouseClickHandler,
    popupOpen: false,
  };

  destination.addEventListener(
    'click',
    (<any>destination)._extPopup.mouseClickHandler
  );

  function mouseClickHandler(e: MouseEvent) {
    e.stopPropagation();
    e.preventDefault();
    if ((<any>destination)._extPopup.popupOpen) {
      (<any>destination)._extPopup.popupOpen = false;
      return mouseLeaveHandler();
    }
    (<any>destination)._extPopup.popupOpen = true;
    return mouseEnterHandler();
  }

  function mouseEnterHandler() {
    deleteAllExistingInstancesOfPopper();
    tooltipEl = createTooltipElementContainerInDOM(props.html);
    const closeBtn = tooltipEl.querySelector('.ext-popup-close');
    if (closeBtn) {
      closeBtn.addEventListener(
        'click',
        (<any>destination)._extPopup.mouseClickHandler
      );
    }
    tooltipEl.className = POPPER_CLASS_NAME;
    new Popper(destination, tooltipEl, {
      placement: props.placement,
    } as PopperOptions);

    if (props.afterRender) {
      props.afterRender(tooltipEl, data);
    }
  }

  function mouseLeaveHandler() {
    tooltipEl.remove();
  }
}

export function deleteAllExistingInstancesOfPopper() {
  Array.from(document.querySelectorAll(`.${POPPER_CLASS_NAME}`)).forEach((el) =>
    el.remove()
  );
}

const CONTAINER_PADDING = '15px';

addStyleString(
  `
  .ext-heading-container{
    align-items: center;
    box-shadow: 0px 0px 4px rgba(0, 0, 0, 0.25);
    display: flex;
    flex-direction: row;
    padding: 12px ${CONTAINER_PADDING};
  }
  .ext-heading-container > .ext-heading{
    flex-grow: 4;
  }
  .ext-heading-container h3{
    margin: 0;
  }
  .ext-popup-close{
    height: 24px;
    cursor: pointer;
    transition: background-color 0.3s ease; /* Smooth transition for background color */
  }
  .ext-popup-close:hover {
    background-color: #ddd; /* New background color on hover */
  }
  .ext-info-box{
    background: linear-gradient(180deg, #FFFFFF 0%, #FAFAFA 100%);
    mix-blend-mode: normal;
    border: 1px solid #EFF1F4;
    box-sizing: border-box;
    box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.1);
    padding: ${CONTAINER_PADDING};

    font-family: Open Sans;
    font-style: normal;
    font-weight: 600;
    font-size: 16px;
    line-height: 22px;
  }
  .ext-info-box h3{
    font-family: Open Sans;
    font-style: normal;
    font-weight: 800;
    font-size: 16px;
    line-height: 22px;
    color: #5C5B70;
  }
  .ext-body-container{
    padding: ${CONTAINER_PADDING};
  }
  .ext-body-container li{
    display: flex;
    justify-content: space-between; /* Align children to the ends */
    align-items: center; /* Align children vertically */
    border-bottom: 1px solid #E0E0E0;
    padding: ${CONTAINER_PADDING} 0;
  }
  .ext-body-container li:last-child{
    border-bottom: none;
  }
  .ext-body-container .ext-price,
  .ext-body-container .ext-date{
    flex-grow: 1; /* Equal width */
    text-align: center; /* Center content */
  }
  .ext-body-container .ext-price{
    color: #333; /* Example color */
  }
  .ext-body-container .ext-date{
    color: #555; /* Example color */
  }
  .link-anchor {
    position: relative;
    width: 0;
    font-size: .8em;
    opacity: 0;
    transition: opacity .2s ease-in-out;
  }
  .anchor-wrapper {
    border: none;
  }
  .anchor-wrapper:hover .link-anchor {
    opacity: 1;
  }
  
  section h1[id]:focus,
  section h2[id]:focus,
  section h3[id]:focus,
  section h4[id]:focus,
  section h5[id]:focus {
    outline: 0;
  }
  
  p.thin {
      font-weight: 100;
      margin: 0;
      line-height: 1.2em;
  }
  
  p.bold {
      font-weight: 900;
      margin: 0;
      margin-top: -5px;
  }
  
  .rel {
      width: 30%;
      margin: 0 auto;
      position: relative;
      text-align: center;
      padding: 20px;
      border-style: dotted;
      border-color: white;
      border-width: medium;
  }
  
  .popper,
  .tooltip {
      position: absolute;
      background: #fff;
      color: black;
      border-radius: 3px;
      box-shadow: 0px 0px 4px rgba(0, 0, 0, 0.25);;
      text-align: left;
      direction: ltr;
      z-index: 10000;
      min-width: 250px;
  }
  .style5 .tooltip {
      background: #1E252B;
      color: #FFFFFF;
      max-width: 200px;
      width: auto;
      font-size: .8rem;
      padding: .5em 1em;
  }
  .popper .popper__arrow,
  .tooltip .tooltip-arrow {
      width: 0;
      height: 0;
      border-style: solid;
      position: absolute;
      margin: 5px;
  }
  
  .tooltip .tooltip-arrow,
  .popper .popper__arrow {
      border-color: #aaa;
  }
  .style5 .tooltip .tooltip-arrow {
      border-color: #1E252B;
  }
  .popper[x-placement^="top"],
  .tooltip[x-placement^="top"] {
      margin-bottom: 5px;
  }
  .popper[x-placement^="top"] .popper__arrow,
  .tooltip[x-placement^="top"] .tooltip-arrow {
      border-width: 5px 5px 0 5px;
      border-left-color: transparent;
      border-right-color: transparent;
      border-bottom-color: transparent;
      bottom: -5px;
      left: calc(50% - 5px);
      margin-top: 0;
      margin-bottom: 0;
  }
  .popper[x-placement^="bottom"],
  .tooltip[x-placement^="bottom"] {
      margin-top: 5px;
  }
  .tooltip[x-placement^="bottom"] .tooltip-arrow,
  .popper[x-placement^="bottom"] .popper__arrow {
      border-width: 0 5px 5px 5px;
      border-left-color: transparent;
      border-right-color: transparent;
      border-top-color: transparent;
      top: -5px;
      left: calc(50% - 5px);
      margin-top: 0;
      margin-bottom: 0;
  }
  .tooltip[x-placement^="right"],
  .popper[x-placement^="right"] {
      margin-left: 5px;
  }
  .popper[x-placement^="right"] .popper__arrow,
  .tooltip[x-placement^="right"] .tooltip-arrow {
      border-width: 5px 5px 5px 0;
      border-left-color: transparent;
      border-top-color: transparent;
      border-bottom-color: transparent;
      left: -5px;
      top: calc(50% - 5px);
      margin-left: 0;
      margin-right: 0;
  }
  .popper[x-placement^="left"],
  .tooltip[x-placement^="left"] {
      margin-right: 5px;
  }
  .popper[x-placement^="left"] .popper__arrow,
  .tooltip[x-placement^="left"] .tooltip-arrow {
      border-width: 5px 0 5px 5px;
      border-top-color: transparent;
      border-right-color: transparent;
      border-bottom-color: transparent;
      right: -5px;
      top: calc(50% - 5px);
      margin-left: 0;
      margin-right: 0;
  }
  .hide{
    display: none;
  }
  `,
  document.body
);

export default popover;
