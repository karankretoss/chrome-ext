const TRENDING_FLAT_SVG = ({size = 20, color = "#fff"}) => `<svg fill="${color}" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="${size}px"
height="${size}px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
<g id="Bounding_Boxes">
<g id="ui_x5F_spec_x5F_header_copy_3">
</g>
<path fill="none" d="M0,0h24v24H0V0z"/>
</g>
<g id="Duotone">
<g id="ui_x5F_spec_x5F_header_copy_2">
</g>
<path d="M22,12l-4-4v3H3v2h15v3L22,12z"/>
</g>
</svg>`;

export default TRENDING_FLAT_SVG;
