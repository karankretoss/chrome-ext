import PopOver from "./popover";
import IHistoricData from "../entities/reusable/types/IHistoricData";
import getParsedNumberString from "../helpers/getParsedNumberString";

// Define CSS classes for common styles
const styles = {
  headingContainer: `
    margin: 0px; 
    display: flex; 
    align-items: center;
  `,
  label: `
    width: 54px; 
    height: 40px; 
    line-height: 40px; 
    text-align: center; 
    background: linear-gradient(180deg, #E25950 0%, #C23D4B 100%); 
    box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.05); 
    border-radius: 20px 0px 0px 20px; 
    font-family: Open Sans; 
    font-style: normal; 
    font-weight: 600; 
    font-size: 16px; 
    color: #fff;
  `,
  value: `
    width: 72px; 
    height: 38px; 
    line-height: 36px; 
    text-align: center; 
    background: linear-gradient(180deg, #FFFFFF 0%, #F8F8F8 100%); 
    border: 1px solid #E1E5EA; 
    box-sizing: border-box; 
    box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.05); 
    border-radius: 0px 20px 20px 0px; 
    font-family: Open Sans; 
    font-style: normal; 
    font-weight: 600; 
    font-size: 16px; 
    color: #959595;
  `,
};

// Parse date once
function parseDate(date: string): string {
  const options: Intl.DateTimeFormatOptions = { year: 'numeric', month: '2-digit', day: '2-digit' };
  const dateObj = new Date(date);
  return dateObj.toLocaleDateString('en-UK', options);
}

// Generate HTML content
function generateContent(serverData: IHistoricData): string {
  if (serverData.changes.length === 0) {
    return `Broken data`;
  }

  const formattedChanges = serverData.changes
    .slice()
    .reverse()
    .map((change: any) => `
      <li>
        <div class="ext-price">
          ${change.price.currency !== "?" ? getParsedNumberString({ floatNumber: change.price.amount, symbol: change.price.currency }) : "No price"}
        </div>
        <div class="ext-date">
          ${parseDate(change.date)}
        </div>
      </li>
    `).join("");

  return `
    <div class="ext-heading-container">
      <h3 class="ext-heading">
        <div style="${styles.headingContainer}">
          <div style="${styles.label}">Rote</div>
          <div style="${styles.value}">Kapsel</div>
        </div>
      </h3>
      <div class="ext-popup-close">
        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" clip-rule="evenodd" d="M18 7.05L16.95 6L12 10.95L7.05 6L6 7.05L10.95 12L6 16.95L7.05 18L12 13.05L16.95 18L18 16.95L13.05 12L18 7.05Z" fill="black"/>
        </svg>
      </div>
    </div>
    <div class="ext-body-container">
      <div class="ext-info-box">
        <h3>Erstmals erfasst</h3>
        ${parseDate(serverData.changes[0].date)}
      </div>
      <ul>${formattedChanges}</ul>
    </div>
  `;
}

// Export function
export default function putMoreInfoPopover({
  domElement,
  historicData,
}: {
  domElement: HTMLDivElement;
  historicData: IHistoricData;
}): void {
  PopOver(
    domElement,
    {
      placement: "bottom",
      html: generateContent(historicData),
    },
    {
      serverData: historicData,
    }
  );
}
