let lastURL = window.location.href;

const onEachPathChange = (callback: Function): void => {
  setInterval(async () => {
    if (window.location.href !== lastURL) {
      lastURL = window.location.href;
      await callback();
    }
  }, 50);
};

export default onEachPathChange;
