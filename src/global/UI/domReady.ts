export default function domReady(){
    return new Promise(function(resolve){
        if (
            document.readyState === "complete" ||
            (document.readyState !== "loading" )
        ) {
            resolve(undefined);
        } else {
          document.addEventListener("DOMContentLoaded", function(){
            resolve(undefined);
          });
        }
    });
}
