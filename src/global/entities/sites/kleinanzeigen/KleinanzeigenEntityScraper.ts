import KleinanzeigenEntityBuilder from "./KleinanzeigenEntityBuilder";
import EntityScraper, { IHighScraperProps } from "../../reusable/site/EntityScraper";
import {Source} from "../../reusable/types/Source";

const entitiesDOMNodesSelector: string = ".aditem[data-adid]"
export default class KleinanzeigenEntityScraper extends EntityScraper {
    public source = Source.kleinanzeigen;
    async getInPageJSONData(): Promise<unknown> {
        return;
    }
    constructor({ dealType, realestateType } : IHighScraperProps) {
        super({
                Builder: KleinanzeigenEntityBuilder,
                entitiesDOMNodesSelector,
                dealType,
                realestateType,      
                readinessConditionFunction: () => Array.from(document.querySelectorAll(entitiesDOMNodesSelector)).length > 3
            }
        )
    }
}
