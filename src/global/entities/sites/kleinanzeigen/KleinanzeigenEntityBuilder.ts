import EntityBuilder from "../../reusable/site/EntityBuilder";
import { DealType, RealestateType } from "../../rotekapsel";
import KleinanzeigenEntityNormal from "./KleinanzeigenEntityNormal";

export default class KleinanzeigenEntityBuilder extends EntityBuilder {
  protected entityNormal = KleinanzeigenEntityNormal;
  protected shouldShowIndicator = false;

  getDealAndRealestateTypesFromText(_text: string): [ any, any ]  {
    return [ null, null];
  }

  getDealAndRealestateTypesEmptyHandler(dealType: DealType, realestateType: RealestateType): [ DealType, RealestateType] {
      return [dealType, realestateType]
  }

}
