import {
    FullPageScrapeSelector,
    PropertySelectorAndTextParser,
    ScrapedEntityWithAdditionalProperties
} from "../../reusable/types/ScrapedEntity";
import EntityNormal from "../../reusable/site/EntityNormal";
import {ImmoScoutParsePriceAsPrice, ImmoscoutGetParseMeasurement} from "../../../helpers/ImmoScoutParsePriceNodeAsPrice";
import {getZipCodeFromExtraProperties} from "../../reusable/utils/misc";
import Measurement from "../../Price";

export default class KleinanzeigenEntityNormal extends EntityNormal {
    // Don't need link node selector, since it's the root node itself (entityDiv)
    protected linkNodeSelectors: PropertySelectorAndTextParser[] = [{
        selector: '.aditem-image a'
    }]
    protected extraPropertiesSelector: FullPageScrapeSelector = {
        title: [{
            selector: '.text-module-begin'
        }],
        purchasePrice: [{
            selector: '.aditem-main--middle--price-shipping--price',
            measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
        }],
        address: [{
            selector: '.aditem-main--top--left'
        }]
    }
    get guid(): string | undefined {
        return this.props.entityDiv.getAttribute('data-adid') as string | undefined
    }

    get pricesNodes(): HTMLDivElement[] {
        try {
            const priceNodes: NodeListOf<HTMLDivElement> = this.props.entityDiv.querySelectorAll(
                '.aditem-main--middle--price-shipping--price'
            );
            return Array.from(priceNodes).reduce((acc: HTMLDivElement[], elm: HTMLDivElement) => {
                if(elm.innerText.indexOf('€') > -1){
                    acc.push(elm);
                }
                return acc;
            }, []);
        } catch (_) {
            return [];
        }
    }

    get prices(): Measurement[] {
        const checkDict: Record<string, boolean> = {};
        return this.pricesNodes.reduce((acc: Measurement[],elm: HTMLDivElement) => {
            const tmpPprice = ImmoScoutParsePriceAsPrice(elm.innerText, " ");
            const price =  tmpPprice && {
                ...tmpPprice,
                label: this.props.dealType === 'rent' ? (this.props.realestateType === 'parking' ? 'Miete' : 'Warmmiete') : 'Kaufpreis',
            };
            if(price){
                // use map to find if already exists and decide if to push
                const key = `${price.number}:${price.measure}:${price.label}`;
                if (!checkDict[key]) {
                    acc.push(price);
                    checkDict[key] = true;
                }
            }
            return acc;
        }, []);
    }

    // Sample data obj https://jsoneditoronline.org/#left=cloud.add3cc543b50477586939d5df29a9f0c
    adjustExtraProperties(currentResult: ScrapedEntityWithAdditionalProperties): ScrapedEntityWithAdditionalProperties {
        if (!currentResult.zipCode) {
            currentResult.zipCode = getZipCodeFromExtraProperties(currentResult)
        }
        return currentResult;
    }
}
