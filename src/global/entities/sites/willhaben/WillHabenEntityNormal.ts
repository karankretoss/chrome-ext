import {
    FullPageScrapeSelector,
    PropertySelectorAndTextParser,
    ScrapedEntityWithAdditionalProperties
} from "../../reusable/types/ScrapedEntity";
import EntityNormal from "../../reusable/site/EntityNormal";
import {ImmoscoutGetParseMeasurement, WillHabenParsePriceAsPrice} from "../../../helpers/ImmoScoutParsePriceNodeAsPrice";
import {getLastArrayElement, getZipCodeFromExtraProperties} from "../../reusable/utils/misc";
import Measurement from "../../Price";

export default class WillHabenEntityNormal extends EntityNormal {
    // Don't need link node selector, since it's the root node itself (entityDiv)
    protected linkNodeSelectors: PropertySelectorAndTextParser[] = []
    protected extraPropertiesSelector: FullPageScrapeSelector = {
        title: [{
            selector: 'h3'
        }],
        purchasePrice: [{
            selector: '[data-testid*="search-result-entry-price"]',
            measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
        }],
        address: [{
            selector: '[data-testid*="search-result-entry-subheader"] > *[aria-label]'
        }]
    }

    get linkNode(): HTMLAnchorElement | null {
        return this.props.entityDiv as unknown as HTMLAnchorElement;
    }
    // eslint-disable-next-line getter-return
    get guid(): string | undefined {
        const linkNode = this.linkNode;
        if (linkNode) {
            let pathname = (((linkNode as any) as HTMLAnchorElement).pathname);
            if (pathname.endsWith('/'))
                pathname = pathname.substring(0, pathname.length - 1)
            const lastPath = getLastArrayElement(pathname.split('/'));
            return getLastArrayElement(lastPath.split('-'))
        }
    }

    get pricesNodes(): HTMLDivElement[] {
        try {
            const priceNodes: NodeListOf<HTMLDivElement> = this.props.entityDiv.querySelectorAll(
                '[data-testid*="search-result-entry-price"]'
            );
            return Array.from(priceNodes).reduce((acc: HTMLDivElement[], elm: HTMLDivElement) => {
                if(elm.innerText.indexOf('€') > -1){
                    acc.push(elm);
                }
                return acc;
            }, []);
        } catch (_) {
            return [];
        }
    }

    get prices(): Measurement[] {
        const checkDict: Record<string, boolean> = {};
        const pricesNodes = this.pricesNodes;
        if (pricesNodes.length < 1) {
            return [{
                measure: '?',
                number: 0,
                label: '',
            }];
        }
        return pricesNodes.reduce((acc: Measurement[],elm: HTMLDivElement) => {
            let price = WillHabenParsePriceAsPrice(elm.innerText, " ");
            if(price){
                if (!price.label) {
                    price.label = ( this.props.dealType === 'rent' ? (this.props.realestateType === 'parking' ? 'Miete' : 'Warmmiete') : 'Kaufpreis' );
                }
                // use map to find if already exists and decide if to push
                const key = `${price.number}:${price.measure}:${price.label}`;
                if (!checkDict[key]) {
                    acc.push(price);
                    checkDict[key] = true;
                }
            }
            return acc;
        }, []);
    }

    // Sample data obj https://jsoneditoronline.org/#left=cloud.add3cc543b50477586939d5df29a9f0c
    adjustExtraProperties(currentResult: ScrapedEntityWithAdditionalProperties): ScrapedEntityWithAdditionalProperties {
        if (!currentResult.zipCode) {
            currentResult.zipCode = getZipCodeFromExtraProperties(currentResult)
        }
        return currentResult;
    }
}
