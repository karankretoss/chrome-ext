import EntityBuilder from "../../reusable/site/EntityBuilder";
import WillHabenEntityNormal from "./WillHabenEntityNormal";

export default class WillHabenEntityBuilder extends EntityBuilder {
  protected entityNormal = WillHabenEntityNormal;
  protected shouldShowIndicator = false;
}
