import WillHabenEntityBuilder from "./WillHabenEntityBuilder";
import EntityScraper, { IHighScraperProps } from "../../reusable/site/EntityScraper";
import {Source} from "../../reusable/types/Source";

const entitiesDOMNodesSelector: string = "#skip-to-resultlist a[data-testid*=search-result-entry-header]"
export default class WillHabenEntityScraper extends EntityScraper {
    public source = Source.willHaben;
    async getInPageJSONData(): Promise<unknown> {
        return;
    }
    constructor({ dealType, realestateType } : IHighScraperProps) {
        super({
                Builder: WillHabenEntityBuilder,
                entitiesDOMNodesSelector,
                dealType,
                realestateType,
                readinessConditionFunction: () => Array.from(document.querySelectorAll(entitiesDOMNodesSelector)).length > 3
            }
        )
    }
}
