import {
    FullPageScrapeSelector,
    PropertySelectorAndTextParser,
    ScrapedEntityWithAdditionalProperties
} from "../../../reusable/types/ScrapedEntity";
import EntityNormal from "../../../reusable/site/EntityNormal";
import {ImmoscoutGetParseMeasurement} from "../../../../helpers/ImmoScoutParsePriceNodeAsPrice";
import {getZipCodeFromExtraProperties} from "../../../reusable/utils/misc";
import Measurement from "../../../Price";
import getNumberFromPrice from "../../../../helpers/getNumberFromPrice";

export default class ImmoScoutPageEntityNormal extends EntityNormal {
    protected linkNodeSelectors: PropertySelectorAndTextParser[] = [{selector: 'a'}]
    protected extraPropertiesSelector: FullPageScrapeSelector = {
        title: [{
            selector: 'h2'
        }],
        purchasePrice: [{
            selector: '.Text-color-gray-dark-wi_',
            measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
        }],
        address: [{
            selector: 'address'
        }]
    }

    get pricesNodes(): HTMLDivElement[] {
        try {
            const clonedDiv = this.props.entityDiv.cloneNode(true) as HTMLElement;
            const maybePriceNodes: NodeListOf<HTMLDivElement> = clonedDiv.querySelectorAll(
                '[class*=PriceKeyFacts] li'
            );
            return Array.from(maybePriceNodes).reduce((acc: HTMLDivElement[], elm: HTMLDivElement) => {
                if(elm.innerText.indexOf('€') > -1){
                    acc.push(elm);
                }
                return acc;
            }, []);
        } catch (_) {
            return [];
        }
    }

    private ISATParsePrice(
        price: string,
        priceCurrencySeparator: string
      ): Measurement | undefined {
        try {
          const priceArr = price.split(priceCurrencySeparator);
          if (priceArr.length > 2) {
            priceArr.shift();
          }
          if (priceArr.length !== 2) {
            return undefined;
          }
          return {
            number: getNumberFromPrice(priceArr[0]),
            measure: priceArr[1],
            label: "",
          };
        } catch (_) {return undefined}
    }

    private ISATParsePriceAsPrice(
        priceNode: HTMLDivElement,
        priceCurrencySeparator: string,
        dealType: string,
    ): Measurement | undefined {
        try {
            const price = this.ISATParsePrice(priceNode.innerText, priceCurrencySeparator);
            if (!price || Number.isNaN(price.number)) {
              return undefined;
            }
            let { measure } = price;
            if (!(measure === '€/m²' || measure === '€')) {
                return undefined;
            }
            let label: string;
            if (measure === '€/m²') {
                measure = '€';
                label = dealType === 'rent' ? 'Mietpreis/m²' : 'pricePerSquareMetre';
            } else {
                label = dealType === 'rent' ? 'Warmmiete' : 'Kaufpreis';
            }
            return {
                number: price.number,
                measure,
                label,
            };
          } catch (_) {return undefined}
    }

    get prices(): Measurement[] {
        const checkDict: Record<string, boolean> = {};
        return this.pricesNodes.reduce((acc: Measurement[],elm: HTMLDivElement) => {
            const price = this.ISATParsePriceAsPrice(elm, " ", this.props.dealType);
            if(price){
                // use map to find if already exists and decide if to push
                const key = `${price.number}:${price.measure}:${price.label}`;
                if (!checkDict[key]) {
                    acc.push(price);
                    checkDict[key] = true;
                }
            }
            return acc;
        }, []);
    }

    // Sample data obj https://jsoneditoronline.org/#left=cloud.add3cc543b50477586939d5df29a9f0c
    adjustExtraProperties(currentResult: ScrapedEntityWithAdditionalProperties): ScrapedEntityWithAdditionalProperties {
        if (!currentResult.zipCode) {
            currentResult.zipCode = getZipCodeFromExtraProperties(currentResult)
        }
        return currentResult;
    }
}
