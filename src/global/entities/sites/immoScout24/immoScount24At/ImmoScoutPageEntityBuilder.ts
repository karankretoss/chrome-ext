import EntityBuilder from "../../../reusable/site/EntityBuilder";
import ImmoScoutPageEntityNormal from "./ImmoScoutPageEntityNormal";

export default class ImmoScoutPageEntityBuilder extends EntityBuilder {
  protected entityNormal = ImmoScoutPageEntityNormal;
  protected shouldShowIndicator = false;
}
