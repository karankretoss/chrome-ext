import ImmoScoutPageEntityBuilder from "./ImmoScoutPageEntityBuilder";
import EntityScraper, { IHighScraperProps } from "../../../reusable/site/EntityScraper";
import {Source} from "../../../reusable/types/Source";
export default class ImmoScout24EntityScraper extends EntityScraper {
    public source = Source.immoScout24At;
    async getInPageJSONData(): Promise<unknown> {
        return;
    }
    constructor({ dealType, realestateType } : IHighScraperProps) {
        super({
                Builder: ImmoScoutPageEntityBuilder,
                entitiesDOMNodesSelector: '[data-testid="results-items"] > li',
                dealType,
                realestateType,
                readinessConditionFunction: () => Array.from(document.querySelectorAll(".result-list-entry")).length > 3
            }
        )
    }
}
