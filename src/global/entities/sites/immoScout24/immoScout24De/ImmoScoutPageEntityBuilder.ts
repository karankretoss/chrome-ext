import IPageEntity from "../../../reusable/types/IPageEntity";
import ImmoScoutPageEntityNormal from "./ImmoScoutPageEntityNormal";
import ImmoScoutPageEntityGrouped from "./ImmoScoutPageEntityGrouped";
import IndicatorNormal from "../../../reusable/types/Indicator/IndicatorNormal";
import IndicatorGrouped from "../../../reusable/types/Indicator/IndicatorGrouped";
import ImmoScoutPageEntityExpose from "./ImmoScoutPageEntityExpose";
import IScrapeBuilderOptions from "../../../reusable/types/IScrapeBuilderOptions";
import type {DealType, RealestateType} from "../../../reusable/utils/rotekapsel";
import ImmoScoutPageEntityNormalProject from "./ImmoScoutPageEntityNormalProject";
import EntityBuilder, {handleListPageParams} from "../../../reusable/site/EntityBuilder";

export default class ImmoScoutPageEntityBuilder extends EntityBuilder {
  protected entityNormal = ImmoScoutPageEntityNormal;
  getDealAndRealestateTypesEmptyHandler(dealType: DealType, realestateType: RealestateType): [ DealType, RealestateType] {
    if (!(dealType && realestateType)) {
      try {
        const bcText = document.querySelector(".breadcrumb__item:first-child a")?.textContent || "";
        return this.getDealAndRealestateTypesFromText(bcText);
      } catch (_) {
        return [dealType, realestateType]
      }
    }
    return [dealType, realestateType]
  }

  handleMultipleListingsInOneNode(multipleListingsInOneNode: Element, dealType: DealType, realestateType: RealestateType): IPageEntity[] {
    const groupedEntities = multipleListingsInOneNode.querySelectorAll(
        ".grouped-listing-frame"
    );
    return Array.from(groupedEntities).map((groupedEntity) => {
      const entity = new ImmoScoutPageEntityGrouped(
          groupedEntity as HTMLDivElement,
          dealType,
          realestateType,
      );
      const indicatorParent = groupedEntity.querySelector("div");
      indicatorParent?.setAttribute('style', `${indicatorParent.style};position: relative;`);
      const indicator = new IndicatorGrouped(
          indicatorParent as HTMLDivElement,
          { top: "3px", left: "3px", marginTop: "0px" }
      );
      entity.indicator = indicator;
      return entity;
    });
  }

  handleExposePage(dealType: DealType, realestateType: RealestateType): IPageEntity[] {
    try {
      const exposeEntity = new ImmoScoutPageEntityExpose(
          document.querySelector("#is24-main") as HTMLDivElement,
          dealType,
          realestateType,
      );
      if (exposeEntity.isInactive) {
        return [];
      }
      const indicatorParent = exposeEntity.indicatorPlacement;
      if (indicatorParent) {
        exposeEntity.indicator = new IndicatorNormal(
          indicatorParent as HTMLDivElement,
          { left: "10px" }
        );
      }

      return [exposeEntity];
    } catch (error: any) {
      console.error(`Could not create ImmoScoutPageEntityExpose due to '${error.message}'`);
      return [];
    }
  }

  handleListPage(params: handleListPageParams): IPageEntity[] {
    const {entityDOMNode, dealType, realestateType, isProjectAds} = params;
    try {
      const entity = !isProjectAds ? new this.entityNormal({
        entityDiv: entityDOMNode as HTMLDivElement,
        dealType,
        realestateType,
      }) :  new ImmoScoutPageEntityNormalProject({
        entityDiv: entityDOMNode as HTMLDivElement,
        dealType,
        realestateType,
      });
      if (entity.scrapedEntity) {
        const indicator = new IndicatorNormal(
            entity.indicatorPlacement as HTMLDivElement
        );
        entity.indicator = indicator;
      }
      return [entity];
    } catch (error: any) {
      console.error(`Could not create ImmoScoutPageEntityNormal due to '${error.message}'`);
      return [];
    }
  }

  build({ entityDOMNode, isProjectAds }: IScrapeBuilderOptions): IPageEntity[] {
    if (!entityDOMNode) {
      return [];
    }

    const [ dealType, realestateType ] = this.getDealAndRealestateTypes();

    const multipleListingsInOneNode = entityDOMNode.querySelector(
      ".result-list-entry__grouped-listings"
    );
    if (multipleListingsInOneNode?.querySelectorAll('.grouped-listing-frame').length) {
      return this.handleMultipleListingsInOneNode(multipleListingsInOneNode, dealType, realestateType)
    }

    const exposePage = entityDOMNode.querySelector("#is24-main");
    if (exposePage) {
      return this.handleExposePage(dealType, realestateType)
    }

    return this.handleListPage({entityDOMNode, dealType, realestateType, isProjectAds})
  }
}
