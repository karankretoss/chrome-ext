import getIdFromPath from "../../../../helpers/getIdFromPath";
import {ImmoScoutParsePriceAsPrice, ImmoscoutGetParseMeasurement} from "../../../../helpers/ImmoScoutParsePriceNodeAsPrice";
import IPageEntity from "../../../reusable/types/IPageEntity";
import Measurement from "../../../reusable/types/Price";
import BaseScrapedEntity, { FullPageScrapeSelector, ScrapedEntityWithAdditionalProperties } from "../../../reusable/types/ScrapedEntity";
import IIndicator from "../../../reusable/types/Indicator/IIndicator";
import type {DealType, RealestateType} from "../../../reusable/utils/rotekapsel";
import { extractProperties } from "../../../../helpers/extractProperties";
import { featuresMap } from "../../../../helpers/features.map";

export default class ImmoScoutPageEntityGrouped implements IPageEntity {
  private entityDiv: HTMLDivElement;
  private dealType: DealType;
  private realestateType: RealestateType;
  // @ts-ignore
  private _indicator: IIndicator;

  protected extraPropertiesSelector: FullPageScrapeSelector = {
    purchasePrice: [{
        selector: '.grid-item.listing-details div.grid-item:first-child span',
        measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
    }],
    price: [{
        selector: '.grid-item.listing-details div.grid-item:first-child span',
        measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
    }],
    livingArea: [{
        selector: '.grid-item.listing-details div.grid-item:nth-child(2) span',
        matchText: '',
        measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
    }],
    room: [{
        selector: '.grid-item.listing-details div.grid-item:nth-child(3) span',
        matchText: 'Zi.',
        measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
    }],
    genericFeatures: [{
      selector: '.result-list-entry__secondary-criteria li'
    }]
  }

  constructor(entityDiv: HTMLDivElement, dealType: DealType, realestateType: RealestateType) {
    this.entityDiv = entityDiv;
    this.dealType = dealType;
    this.realestateType = realestateType;
  }

  set indicator(indicator: IIndicator) {
    this._indicator = indicator;
  }

  get indicatorPlacement(): HTMLDivElement | null {
    return this.entityDiv.querySelector(".need-to-fill-it");
  }


  get extraProperties(): ScrapedEntityWithAdditionalProperties {
    const clonedDiv = this.entityDiv.cloneNode(true) as HTMLElement;
    const result = extractProperties(clonedDiv, this.extraPropertiesSelector);

    // additional handler for special properties
    if (result.merchantName === undefined) {
        const isPrivateCompanyNode = this.entityDiv.querySelector('.result-list-entry__brand-logo--private')
        if (isPrivateCompanyNode) {
            result.merchantName = 'Private';
        }
    }
    if (this.extraPropertiesSelector.genericFeatures !== undefined) {
        result.genericFeatures = Array.from(this.entityDiv.querySelectorAll(this.extraPropertiesSelector.genericFeatures[0].selector))
            .map(node => {
                const val = (node as HTMLElement).innerText || '';
                return featuresMap[val] || val;
            })
            .filter(feature => !['...', ''].includes(feature));
    }
    if (result.sellerName || result.merchantName) {
        result.seller = {
            isMerchant: 'Private' !== result.merchantName,
            sellerName: result.sellerName,
            merchantName: 'Private' === result.merchantName ? '' : result.merchantName,
        }
        delete result.sellerName;
        delete result.merchantName;
    }

    if (!result.title || result.title.length === 0) {
      const parentArticleElement = this.entityDiv.closest('article');
      const h2Element = parentArticleElement?.querySelector('h2');
      if (h2Element) {
        result.title = h2Element.innerText ?? "";
      }
    }
    
    return result
  }

  // eslint-disable-next-line getter-return
  get scrapedEntity(): BaseScrapedEntity | undefined {
    const linkNode = this.linkNode;
    if (linkNode) {
      return {
        ...this.extraProperties,
        guid: this.guid as string,
        path: ((linkNode as any) as HTMLAnchorElement).pathname,
        prices: this.prices,
        dealType: this.dealType,
        realestateType: this.realestateType,
        isFullEntity: false,
        indicator: this.indicator,
      };
    }
  }

  get indicator(): IIndicator {
    return this._indicator;
  }

  // eslint-disable-next-line getter-return
  get guid(): string | undefined {
    const linkNode = this.linkNode;
    if (linkNode) {
      return getIdFromPath(((linkNode as any) as HTMLAnchorElement).pathname);
    }
  }

  get pricesNodes(): HTMLDivElement[] {
    const dataSpans = this.entityDiv.querySelectorAll(
      ".listing-details div.grid-item.four-eighth"
    );
    if (dataSpans.length > 0) {
      return [dataSpans[0] as HTMLDivElement];
    }
    return [];
  }

  get prices(): Measurement[] {
    return this.pricesNodes.reduce((acc: Measurement[], priceNode: HTMLDivElement) => {
      if (priceNode) {
        const price = ImmoScoutParsePriceAsPrice(priceNode.innerText, " ");
        if (price) {
          acc.push(price);
        }
      }
      return acc;
    }, []);
  }

  private get linkNode(): HTMLAnchorElement | null {
    return this.entityDiv.querySelector("a");
  }
}
