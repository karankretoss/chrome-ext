import ImmoScoutPageEntityBuilder from "./ImmoScoutPageEntityBuilder";
import Favorite from "../../../reusable/types/Favorite";
import {isValueOfEntitiesEqual} from "../../../../helpers";
import EntityScraper, { IHighScraperProps } from "../../../reusable/site/EntityScraper";
import {Source} from "../../../reusable/types/Source";

declare global {
  interface Window {
    IS24: any,
  }
}

export default class ImmoScout24EntityScraper extends EntityScraper {
  public source = Source.immoScout24;
  constructor({ dealType, realestateType } : IHighScraperProps) {
    super({
          Builder: ImmoScoutPageEntityBuilder,
          entitiesDOMNodesSelector: "#resultListItems li.result-list__listing",
          exposeDOMNodesSelector: ".viewport",
          dealType,
          realestateType,
          readinessConditionFunction: () => Array.from(document.querySelectorAll(".result-list-entry")).length > 3
        },
    );
  }
  async initializeListPage(): Promise<void> {
    await super.initializeListPage();

    const entitiesDOMNodesForProjectAds: NodeListOf<HTMLDivElement> = document.querySelectorAll(
        `#resultListItems li.result-list__listing [data-testid='grouped-object']`,
    ) || [];
    const projectAdsEntities = this.initializeListPageHelper({
      entitiesDOMNodes: entitiesDOMNodesForProjectAds,
      isProjectAds: true
    })

    this.entities!.push(...projectAdsEntities)
  }

  async getFavorites(): Promise<Favorite[]> {
    return this.entities?.map((entity) => {
      if (!entity.scrapedEntity) {
        return undefined;
      }
      const {extraPropertiesViaDataObj, ...rest} = entity.scrapedEntity as any;
      const fields = Object.keys(extraPropertiesViaDataObj || {})
          .filter(key => !isValueOfEntitiesEqual(extraPropertiesViaDataObj[key], rest[key]));

      if (fields.length) {
        return ({
          id: rest.guid,
          fields,
        }) as Favorite;
      }
      return undefined;
    })
        .filter((favorite) => !!favorite) as Favorite[];
  }
}
