import IPageEntity from "../../../reusable/types/IPageEntity";
import {
    FullPageScrapeSelector,
    FullScrapedEntity,
    PropertySelectorAndTextParser,
    ScrapedEntityWithAdditionalProperties
} from "../../../reusable/types/ScrapedEntity";
import {ImmoscoutGetParseMeasurement} from "../../../../helpers/ImmoScoutParsePriceNodeAsPrice";
import IIndicator from "../../../reusable/types/Indicator/IIndicator";
import {extractProperties} from "../../../../helpers/extractProperties";
import {featuresMap} from "../../../../helpers/features.map";
import DataObj from "../../../reusable/types/DataObjArray";
import EntityNormal from "../../../reusable/site/EntityNormal";

export default class ImmoScoutPageEntityNormal extends EntityNormal implements IPageEntity {
    protected _indicator: IIndicator | undefined;
    protected linkNodeSelectors: PropertySelectorAndTextParser[] = [
        {
            selector: 'a[data-go-to-expose-referrer="RESULT_LIST_LISTING"]'
        }, {
            selector: 'a.result-list-entry__brand-title-container'
        }, {
            selector: 'a[data-go-to-expose-referrer="RESULT_LIST_SURROUNDING_SUBURBS"]'
        }, {
            selector: 'a[data-exp-referrer="RESULT_LIST_LISTING"]'
        }, {
            selector: 'a[data-exp-referrer="RESULT_LIST_SURROUNDING_SUBURBS"]'
        }
    ]
    protected priceSelector = '.result-list-entry__criteria .grid .grid-item';
    protected extraPropertiesSelector: FullPageScrapeSelector = {
        title: [{
            selector: '.result-list-entry__brand-title',
            removeChildSelector: '.result-list-entry__new-flag',
        }, {
            selector: '.grid-item a h4',
        }, {
            selector: 'a h2.result-list-entry__brand-title',
        }],
        purchasePrice: [{
            selector: '.result-list-entry__primary-criterion',
            matchText: 'Kaufpreis',
            measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
        }],
        baseRent: [{
            selector: '.result-list-entry__primary-criterion',
            matchText: 'Kaltmiete',
            measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
        }],
        totalRent: [{
            selector: '.result-list-entry__primary-criterion',
            matchText: 'Warmmiete'
        }],
        fullRent: [{
            selector: '.result-list-entry__primary-criterion',
            matchText: 'Gesamtmiete'
        }],
        livingArea: [{
            selector: '.result-list-entry__primary-criterion',
            matchText: 'Wohnfläche',
            measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
        }],
        room: [{
            selector: '.result-list-entry__primary-criterion span.onlySmall',
            matchText: 'Zi.',
            measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
        }],
        approxLandArea:[{
            selector: '.result-list-entry__primary-criterion',
            matchText: 'Grundstück',
            measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
        }] ,
        address: [{
            selector: '.result-list-entry__address',
        }],
        sellerName: [{
            selector: '[data-go-to-expose-hash="/basicContact"] span:first-child'
        }],
        merchantName: [{
            selector: '[data-go-to-expose-hash="/basicContact"] span:last-child'
        }],
        genericFeatures: [{
            selector: '.result-list-entry__secondary-criteria li'
        }]
    }

    get dataObj(): DataObj | undefined {
        if (window.adsData) {
            try {
                let dataObjArray: DataObj[] | undefined = window.adsData.resultList.resultListModel.searchResponseModel['resultlist.resultlist'].resultlistEntries[0].resultlistEntry
                if (!dataObjArray)
                    return undefined
                // Add similar objs
                dataObjArray.forEach(o => {
                    if (o.similarObjects)
                    try {
                        const objects = o.similarObjects.map((oo: any) => oo.similarObject)
                        dataObjArray = [...dataObjArray!,...objects]
                    } catch (_) {}
                })
                if (Array.isArray(dataObjArray)) {
                    return dataObjArray.find(obj => obj["@id"] === this.guid);
                } else if (dataObjArray && dataObjArray["@id"] === this.guid) {
                    return dataObjArray;
                }
            } catch (_) {return undefined}
        }
        return undefined;
    }

    // Sample data obj https://jsoneditoronline.org/#left=cloud.add3cc543b50477586939d5df29a9f0c
    get extraPropertiesViaDataObj(): Partial<ScrapedEntityWithAdditionalProperties> | undefined {
        const dataObj = this.dataObj;
        if (!dataObj) {
            return undefined;
        }
        const dataObjRealEstate = dataObj["resultlist.realEstate"] || {};

        const getSellerName = () => {
            const result = `${dataObjRealEstate?.contactDetails?.firstname || ''}${dataObjRealEstate?.contactDetails?.lastname || ''}`;
            return result || undefined;
        }

        const getAdditionalCost = () => {
            if (dataObj.monthlyRate?.additionalCosts?.number)
                return {
                    number: dataObj.monthlyRate?.additionalCosts?.number,
                    measure: '',
                    label: ''
                }
            return undefined
        }

        const getPropertyAddress = () => {
            const { description, ...rest } = dataObjRealEstate.address || {}
            // @ts-ignore
            rest.country = window?.adsData?.resultList?.gacCountry
            if (JSON.stringify(rest) === '{}') {
                return undefined
            }
            return rest;
        }

        const getBaseRent = () => {
            if (dataObjRealEstate?.price?.marketingType  === 'RENT' && dataObjRealEstate?.price?.priceIntervalType === 'MONTH') {
                return ({
                    number: dataObjRealEstate.price.value,
                    measure: '',
                    label: '',
                });
            }
            return undefined;
        }

        const getTotalRent = () => {
            if (dataObjRealEstate?.calculatedTotalRent?.totalRent?.value) {
                return ({
                    number: dataObjRealEstate.calculatedTotalRent.totalRent.value,
                    measure: '',
                    label: '',
                });
            }
            return undefined;
        }

        const getPurchasePrice = () => {
            if (dataObjRealEstate?.price?.marketingType  === 'PURCHASE' && dataObjRealEstate?.price?.priceIntervalType === 'ONE_TIME_CHARGE') {
                return ({
                    number: dataObjRealEstate.price.value,
                    measure: '',
                    label: '',
                });
            }
            return undefined;
        }

        const result: Partial<ScrapedEntityWithAdditionalProperties> = {
            title: dataObjRealEstate.title,
            address: dataObjRealEstate?.address?.description?.text,
            propertyAddress: getPropertyAddress(),
            sellerName: getSellerName(),
            merchantName: dataObjRealEstate.realtorCompanyName,

            floor: undefined,
            livingArea: dataObjRealEstate.livingSpace && {
                number: dataObjRealEstate.livingSpace,
                measure: '',
                label: ''
            },
            usableArea: undefined,
            approxLandArea: undefined,
            room: dataObjRealEstate.numberOfRooms && {
                number: dataObjRealEstate.numberOfRooms,
                measure: '',
                label: ''
            },
            bedroom: undefined,
            bathroom: undefined,
            baseRent: getBaseRent(),
            totalRent: getTotalRent(),
            additionalCosts: getAdditionalCost(),
            fullRent: undefined,
            purchasePrice: getPurchasePrice(),
            seller: undefined,
        }

        return result;
    }

    get extraProperties(): ScrapedEntityWithAdditionalProperties {
        const clonedDiv = this.props.entityDiv.cloneNode(true) as HTMLElement;
        const result = extractProperties(clonedDiv, this.extraPropertiesSelector);

        if (this.dataObj) {
            const extraPropertiesViaDataObj = this.extraPropertiesViaDataObj
            if (extraPropertiesViaDataObj)
                Object.keys(extraPropertiesViaDataObj)
                    .forEach(key => {
                        const _key = key as keyof ScrapedEntityWithAdditionalProperties
                        if (extraPropertiesViaDataObj[_key] !== undefined && result[_key] === undefined)
                        { // @ts-ignore
                            result[_key] = extraPropertiesViaDataObj[_key]
                        }
                    })
        }

        // additional handler for special properties
        if (result.merchantName === undefined) {
            const isPrivateCompanyNode = this.props.entityDiv.querySelector('.result-list-entry__brand-logo--private')
            if (isPrivateCompanyNode) {
                result.merchantName = 'Private';
            }
        }
        if (this.extraPropertiesSelector.genericFeatures !== undefined) {
            result.genericFeatures = Array.from(this.props.entityDiv.querySelectorAll(this.extraPropertiesSelector.genericFeatures[0].selector))
                .map(node => {
                    const val = (node as HTMLElement).innerText || '';
                    return featuresMap[val] || val;
                })
                .filter(feature => !['...', ''].includes(feature));
        }
        if (result.sellerName || result.merchantName) {
            result.seller = {
                isMerchant: 'Private' !== result.merchantName,
                sellerName: result.sellerName,
                merchantName: 'Private' === result.merchantName ? '' : result.merchantName,
            }
            delete result.sellerName;
            delete result.merchantName;
        }
        return result
    }

    get scrapedEntity(): FullScrapedEntity | undefined {
        const temp = this.scrapeEntityHelper();
        if (temp) {
            return {
                extraPropertiesViaDataObj: this.extraPropertiesViaDataObj,
                ...temp
            }
        }
    }
}
