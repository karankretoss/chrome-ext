import {FullPageScrapeSelector, PropertySelectorAndTextParser, ScrapedEntityWithAdditionalProperties} from "../../../reusable/types/ScrapedEntity";
import {extractProperties} from "../../../../helpers/extractProperties";
import {featuresMap} from "../../../../helpers/features.map";
import ImmoScoutPageEntityNormal from './ImmoScoutPageEntityNormal'
import {ImmoscoutGetParseMeasurement} from "../../../../helpers/ImmoScoutParsePriceNodeAsPrice";

export default class ImmoScoutPageEntityNormalProject extends ImmoScoutPageEntityNormal {
    protected priceSelector = ".grid-item"
    protected extraPropertiesSelector: FullPageScrapeSelector = {
        purchasePrice: [{
            selector: '.grid-item.listing-details div.grid-item:first-child span',
            measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
        }],
        price: [{
            selector: '.grid-item.listing-details div.grid-item:first-child span',
            measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
        }],
        livingArea: [{
            selector: '.grid-item.listing-details div.grid-item:nth-child(2) span',
            matchText: '',
            measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
        }],
        room: [{
            selector: '.grid-item.listing-details div.grid-item:nth-child(3) span',
            matchText: 'Zi.',
            measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
        }],
    }

    protected linkNodeSelectors: PropertySelectorAndTextParser[] = [
        {
            selector: 'a[data-go-to-expose-referrer="RESULT_LIST_GROUPED"]'
        }, {
            selector: 'a[data-go-to-expose-referrer="RESULT_LIST_LISTING_HOMEBUILDER_GROUPED"]'
        }, {
            selector: '.grid-item.listing-details a[data-exp-referrer="RESULT_LIST_GROUPED"]'
        }, {
            selector: 'a[data-exp-referrer="RESULT_LIST_LISTING_HOMEBUILDER_GROUPED"]'
        }
    ]
    get guid(): string | undefined {
        const linkNode = this.linkNode;
        if (linkNode) {
            return linkNode.getAttribute('data-go-to-expose-id') || undefined
        }
    }
    get indicatorPlacement(): HTMLDivElement | null {
        return this.props.entityDiv;
    }

    get extraProperties(): ScrapedEntityWithAdditionalProperties {
        const clonedDiv = this.props.entityDiv.cloneNode(true) as HTMLElement;
        const result = extractProperties(clonedDiv, this.extraPropertiesSelector);

        if (this.dataObj) {
            const extraPropertiesViaDataObj = this.extraPropertiesViaDataObj
            if (extraPropertiesViaDataObj)
                Object.keys(extraPropertiesViaDataObj)
                    .forEach(key => {
                        const _key = key as keyof ScrapedEntityWithAdditionalProperties
                        if (extraPropertiesViaDataObj[_key] !== undefined && result[_key] === undefined)
                        { // @ts-ignore
                            result[_key] = extraPropertiesViaDataObj[_key]
                        }
                    })
        }

        // additional handler for special properties
        if (result.merchantName === undefined) {
            const isPrivateCompanyNode = this.props.entityDiv.querySelector('.result-list-entry__brand-logo--private')
            if (isPrivateCompanyNode) {
                result.merchantName = 'Private';
            }
        }
        if (this.extraPropertiesSelector.genericFeatures !== undefined) {
            result.genericFeatures = Array.from(this.props.entityDiv.querySelectorAll(this.extraPropertiesSelector.genericFeatures[0].selector))
                .map(node => {
                    const val = (node as HTMLElement).innerText || '';
                    return featuresMap[val] || val;
                })
                .filter(feature => !['...', ''].includes(feature));
        }
        if (result.sellerName || result.merchantName) {
            result.seller = {
                isMerchant: 'Private' !== result.merchantName,
                sellerName: result.sellerName,
                merchantName: 'Private' === result.merchantName ? '' : result.merchantName,
            }
            delete result.sellerName;
            delete result.merchantName;
        }
        return result
    }
}
