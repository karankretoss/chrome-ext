import IPageEntity from "./reusable/types/IPageEntity";
import BaseScrapedEntity from "./reusable/types/ScrapedEntity";
import Measurement from "./reusable/types/Price";
import getIdFromPath from "../helpers/getIdFromPath";
// import IndicatorNormal from "./IndicatorNormal";
import IIndicator from "./reusable/types/Indicator/IIndicator";
import getNumberFromPrice from "../helpers/getNumberFromPrice";

export default class ImmoNetPageEntityNormal implements IPageEntity {
  private entityDiv: HTMLDivElement;
  // @ts-ignore
  private _indicator: IIndicator;

  constructor(entityDiv: HTMLDivElement) {
    this.entityDiv = entityDiv;
  }

  set indicator(indicator: IIndicator) {
    this._indicator = indicator;
  }

  get indicatorPlacement(): HTMLDivElement | null {
    return this.entityDiv;
  }

  // eslint-disable-next-line getter-return
  get scrapedEntity(): BaseScrapedEntity | undefined {
    const linkNode = this.linkNode;
    if (linkNode) {
      return {
        guid: this.guid as string,
        path: (linkNode as any as HTMLAnchorElement).pathname,
        prices: this.prices,
        isFullEntity: false,
        indicator: this.indicator,
      };
    }
  }

  get indicator(): IIndicator {
    return this._indicator;
  }

  // eslint-disable-next-line getter-return
  get guid(): string | undefined {
    const linkNode = this.linkNode;
    if (linkNode) {
      return getIdFromPath((linkNode as any as HTMLAnchorElement).pathname);
    }
  }

  get pricesNodes(): HTMLDivElement[] {
    const priceNode = this.entityDiv.querySelector(`[data-test="price"]`);
    if (priceNode) {
      return [priceNode as HTMLDivElement];
    }
    return [];
  }

  private get linkNode(): HTMLDivElement | null {
    return this.entityDiv.querySelector("a") as any as HTMLDivElement;
  }

  get prices(): Measurement[] {
    return this.pricesNodes.reduce((acc: Measurement[], priceNode) => {
      const price = parseTextAsPrice(priceNode, " ");
      if (price) {
        acc.push(price);
      }
      return acc;
    }, []);
  }
}

function parseTextAsPrice(
  priceNode: HTMLDivElement,
  priceCurrencySeparator: string
): Measurement | undefined {
  const priceNodeAmount = priceNode;
  if (!priceNodeAmount) {
    return undefined;
  }
  const labelNode = priceNode.querySelector(".hardfactlabel");
  const firstLine = (priceNodeAmount as HTMLDivElement).innerText.trim();
  let label = "";
  if (labelNode) {
    label = (labelNode as HTMLDivElement).innerText.trim();
  }
  const NON_BREAKING_SPACE = String.fromCharCode(8239);

  if (firstLine.indexOf("auf Anfrage") > -1) {
    return undefined;
  }
  // const priceArr = firstLine.split(NON_BREAKING_SPACE);
  let priceArr = firstLine.split(priceCurrencySeparator);
  if (priceArr.length !== 2) {
    return undefined;
  }
  return {
    number: getNumberFromPrice(priceArr[0]),
    measure: priceArr[1],
    label,
  };
}
