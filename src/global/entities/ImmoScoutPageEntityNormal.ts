import IPageEntity from "./IPageEntity";
import {
    ScrapedEntityWithAdditionalProperties,
    FullPageScrapeSelector,
    FullScrapedEntity, PropertySelectorAndTextParser
} from "./ScrapedEntity";
import Measurement from "./Price";
import ImmoScoutParsePriceNodeAsPrice, { ImmoscoutGetParseMeasurement } from "../helpers/ImmoScoutParsePriceNodeAsPrice";
import getIdFromPath from "../helpers/getIdFromPath";
import IIndicator from "./IIndicator";
import {DealType, RealestateType} from "./rotekapsel";
import {extractProperties} from "../helpers/extractProperties";
import { featuresMap } from "../helpers/features.map";
import DataObj from "./DataObjArray";

export default class ImmoScoutPageEntityNormal implements IPageEntity {
    private entityDiv: HTMLDivElement;
    private dealType: DealType;
    private realestateType: RealestateType;
    // @ts-ignore
    private _indicator: IIndicator;

    private extraPropertiesSelector: FullPageScrapeSelector = {
        title: [{
            selector: '.result-list-entry__brand-title',
            removeChildSelector: '.result-list-entry__new-flag',
        }, {
            selector: '.grid-item a h4',
        }],
        purchasePrice: [{
            selector: '.result-list-entry__primary-criterion',
            matchText: 'Kaufpreis',
            measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
        }],
        baseRent: [{
            selector: '.result-list-entry__primary-criterion',
            matchText: 'Kaltmiete',
            measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
        }],
        totalRent: [{
            selector: '.result-list-entry__primary-criterion',
            matchText: 'Warmmiete'
        }],
        fullRent: [{
            selector: '.result-list-entry__primary-criterion',
            matchText: 'Gesamtmiete'
        }],
        livingArea: [{
            selector: '.result-list-entry__primary-criterion',
            matchText: 'Wohnfläche',
            measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
        }],
        room: [{
            selector: '.result-list-entry__primary-criterion span.onlySmall',
            matchText: 'Zi.',
            measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
        }],
        approxLandArea:[{
            selector: '.result-list-entry__primary-criterion',
            matchText: 'Grundstück',
            measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
        }] ,
        address: [{
            selector: '.result-list-entry__address',
        }],
        sellerName: [{
            selector: '[data-go-to-expose-hash="/basicContact"] span:first-child'
        }],
        merchantName: [{
            selector: '[data-go-to-expose-hash="/basicContact"] span:last-child'
        }],
        genericFeatures: [{
            selector: '.result-list-entry__secondary-criteria li'
        }]
    }

    constructor(entityDiv: HTMLDivElement, dealType: DealType, realestateType: RealestateType) {
        this.entityDiv = entityDiv;
        this.dealType = dealType;
        this.realestateType = realestateType;
    }

    set indicator(indicator: IIndicator) {
        this._indicator = indicator;
    }

    get indicatorPlacement(): HTMLDivElement | null {
        return this.entityDiv.querySelector(".gallery-responsive");
    }

    get dataObj(): DataObj | undefined {
        if (window.IS24) {
            try {
                const dataObjArray: DataObj[] | undefined = window.IS24.resultList.resultListModel.searchResponseModel['resultlist.resultlist'].resultlistEntries[0].resultlistEntry
                if (Array.isArray(dataObjArray)) {
                    return dataObjArray.find(obj => obj["@id"] === this.guid);
                } else if (dataObjArray && dataObjArray["@id"] === this.guid) {
                    return dataObjArray;
                }
            } catch (_) {}
        }
        return undefined;
    }

    // Sample data obj https://jsoneditoronline.org/#left=cloud.add3cc543b50477586939d5df29a9f0c
    get extraPropertiesViaDataObj(): Partial<ScrapedEntityWithAdditionalProperties> | undefined {
        const dataObj = this.dataObj;
        if (!dataObj) {
            return undefined;
        }
        const dataObjRealEstate = dataObj["resultlist.realEstate"] || {};

        const getSellerName = () => {
            const result = `${dataObjRealEstate?.contactDetails?.firstname || ''}${dataObjRealEstate?.contactDetails?.lastname || ''}`;
            return result || undefined;
        }

        const getAdditionalCost = () => {
            if (dataObj.monthlyRate?.additionalCosts?.number)
                return {
                    number: dataObj.monthlyRate?.additionalCosts?.number,
                    measure: '',
                    label: ''
                }
            return undefined
        }

        const getPropertyAddress = () => {
            const { description, ...rest } = dataObjRealEstate.address || {}
            // @ts-ignore
            rest.country = window?.IS24?.resultList?.gacCountry
            if (JSON.stringify(rest) === '{}') {
                return undefined
            }
            return rest;
        }

        const getBaseRent = () => {
            if (dataObjRealEstate?.price?.marketingType  === 'RENT' && dataObjRealEstate?.price?.priceIntervalType === 'MONTH') {
                return ({
                    number: dataObjRealEstate.price.value,
                    measure: '',
                    label: '',
                });
            }
            return undefined;
        }

        const getTotalRent = () => {
            if (dataObjRealEstate?.calculatedTotalRent?.totalRent?.value) {
                return ({
                    number: dataObjRealEstate.calculatedTotalRent.totalRent.value,
                    measure: '',
                    label: '',
                });
            }
            return undefined;
        }

        const getPurchasePrice = () => {
            if (dataObjRealEstate?.price?.marketingType  === 'PURCHASE' && dataObjRealEstate?.price?.priceIntervalType === 'ONE_TIME_CHARGE') {
                return ({
                    number: dataObjRealEstate.price.value,
                    measure: '',
                    label: '',
                });
            }
            return undefined;
        }

        const result: Partial<ScrapedEntityWithAdditionalProperties> = {
            title: dataObjRealEstate.title,
            address: dataObjRealEstate?.address?.description?.text,
            propertyAddress: getPropertyAddress(),
            sellerName: getSellerName(),
            merchantName: dataObjRealEstate.realtorCompanyName,

            floor: undefined,
            livingArea: dataObjRealEstate.livingSpace && {
                number: dataObjRealEstate.livingSpace,
                measure: '',
                label: ''
            },
            usableArea: undefined,
            approxLandArea: undefined,
            room: dataObjRealEstate.numberOfRooms && {
                number: dataObjRealEstate.numberOfRooms,
                measure: '',
                label: ''
            },
            bedroom: undefined,
            bathroom: undefined,
            baseRent: getBaseRent(),
            totalRent: getTotalRent(),
            additionalCosts: getAdditionalCost(),
            fullRent: undefined,
            purchasePrice: getPurchasePrice(),
            seller: undefined,
        }

        return result;
    }

    get extraProperties(): ScrapedEntityWithAdditionalProperties {
        const clonedDiv = this.entityDiv.cloneNode(true) as HTMLElement;
        const result = extractProperties(clonedDiv, this.extraPropertiesSelector);

        if (this.dataObj) {
            const extraPropertiesViaDataObj = this.extraPropertiesViaDataObj
            if (extraPropertiesViaDataObj)
                Object.keys(extraPropertiesViaDataObj)
                    .forEach(key => {
                        const _key = key as keyof ScrapedEntityWithAdditionalProperties
                        if (extraPropertiesViaDataObj[_key] !== undefined && result[_key] === undefined)
                        { // @ts-ignore
                            result[_key] = extraPropertiesViaDataObj[_key]
                        }
                    })
        }

        // additional handler for special properties
        if (result.merchantName === undefined) {
            const isPrivateCompanyNode = this.entityDiv.querySelector('.result-list-entry__brand-logo--private')
            if (isPrivateCompanyNode) {
                result.merchantName = 'Private';
            }
        }
        if (this.extraPropertiesSelector.genericFeatures !== undefined) {
            result.genericFeatures = Array.from(this.entityDiv.querySelectorAll(this.extraPropertiesSelector.genericFeatures[0].selector))
                .map(node => {
                    const val = (node as HTMLElement).innerText || '';
                    return featuresMap[val] || val;
                })
                .filter(feature => !['...', ''].includes(feature));
        }
        if (result.sellerName || result.merchantName) {
            result.seller = {
                isMerchant: 'Private' !== result.merchantName,
                sellerName: result.sellerName,
                merchantName: 'Private' === result.merchantName ? '' : result.merchantName,
            }
            delete result.sellerName;
            delete result.merchantName;
        }
        return result
    }

    // eslint-disable-next-line getter-return
    get scrapedEntity(): FullScrapedEntity | undefined {
        const linkNode = this.linkNode;
        const extraProperties = this.extraProperties
        if (linkNode) {
            return {
                ...extraProperties,
                extraPropertiesViaDataObj: this.extraPropertiesViaDataObj,
                guid: this.guid as string,
                path: ((linkNode as any) as HTMLAnchorElement).pathname,
                prices: this.prices,
                dealType: this.dealType,
                realestateType: this.realestateType,
                isFullEntity: false,
                indicator: this.indicator,
            };
        }
    }

    get indicator(): IIndicator {
        return this._indicator;
    }

    // eslint-disable-next-line getter-return
    get guid(): string | undefined {
        const linkNode = this.linkNode;
        if (linkNode) {
            return getIdFromPath(((linkNode as any) as HTMLAnchorElement).pathname);
        }
    }

    get pricesNodes(): HTMLDivElement[] {
        const maybePriceNodes = this.entityDiv.querySelectorAll(
            ".result-list-entry__criteria .grid .grid-item"
        );
        // @ts-ignore
        return Array.from(maybePriceNodes).reduce((acc: HTMLDivElement[], elm: HTMLDivElement) => {
            if(elm.innerText.indexOf('€') > -1){
                acc.push(elm);
            }
            return acc;
        }, []);
    }

    private get linkNode(): HTMLAnchorElement | null {
        const propertySelectors: PropertySelectorAndTextParser[] = [
            {
                selector: 'a[data-go-to-expose-referrer="RESULT_LIST_LISTING"]'
            }, {
                selector: 'a.result-list-entry__brand-title-container'
            }, {
                selector: 'a[data-go-to-expose-referrer="RESULT_LIST_SURROUNDING_SUBURBS"]'
            }
        ]

        for (let sel of propertySelectors)
            if (this.entityDiv.querySelector(sel.selector))
                return this.entityDiv.querySelector(sel.selector)

        return null;
    }

    get prices(): Measurement[] {
        const checkDict: Record<string, boolean> = {};
        return this.pricesNodes.reduce((acc: Measurement[],elm: HTMLDivElement) => {
            const price = ImmoScoutParsePriceNodeAsPrice(elm, " ");
            if(price){
                // use map to find if already exists and decide if to push
                const key = `${price.number}:${price.measure}:${price.label}`;
                if (!checkDict[key]) {
                    acc.push(price);
                    checkDict[key] = true;
                }
            }
            return acc;
        }, []);
    }
}
