import BaseScrapedEntity from "./reusable/types/ScrapedEntity";
import ImmoWeltPageEntityBuilder from "./ImmoWeltPageEntityBuilder";
import IPageEntity from "./reusable/types/IPageEntity";
import IHistoricData from "./reusable/types/IHistoricData";

const wait = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));

export default class ImmoNetEntityScraper {
  // @ts-ignore
  private entities: IPageEntity[];

  async initializeListPage(): Promise<void> {
    await this.waitForPageReady();
    // const projectNodes: NodeListOf<HTMLDivElement> = document.querySelectorAll(
    //   `[class^="ProjectItem"]`
    // );
    const entitiesDOMNodes: NodeListOf<HTMLDivElement> =
      document.querySelectorAll(`[class^="EstateItem"]`);

    this.entities = Array.from(entitiesDOMNodes)
      //.concat(Array.from(projectNodes))
      .map((entitiesDOMNode: HTMLDivElement) =>
        ImmoWeltPageEntityBuilder.build(entitiesDOMNode)
      )
      .reduce((acc: IPageEntity[], entities: IPageEntity[]) => {
        entities.forEach((entity) => acc.push(entity));
        return acc;
      }, [] as IPageEntity[]);
  }

  destroy() {
    this.entities.forEach((entity) => {
      if (entity.indicator)
        entity.indicator.element.remove();
    });
  }

  async initializeExposePage(): Promise<void> {
    await wait(1500);
    this.entities = ImmoWeltPageEntityBuilder.build(
      document.querySelector("body") as any as HTMLDivElement
    );
  }

  async waitForPageReady() {
    return await new Promise((resolve, reject) => {
      let interval = setInterval(() => {
        const imagePlaceholder = document.querySelector(
          `[class^="ImagePlaceholder--loading"]`
        ) as SVGElement;
        if (!imagePlaceholder) {
          clearInterval(interval);
          resolve(true);
        }
      }, 1000);
    });
    // return true;
  }

  async getPageEntities(): Promise<BaseScrapedEntity[]> {
    return this.entities
      .map((entity) => entity.scrapedEntity)
      .filter((scrapedEntity) => scrapedEntity) as BaseScrapedEntity[];
  }

  updateIndicators(historicData: IHistoricData[]): void {
    if (!(historicData && historicData.length)) {
      return;
    }
    historicData.forEach((history: IHistoricData) => {
      const entity = this.entities.find(
        (entity) => entity.guid === history.guid
      );
      if (entity && entity.indicator) {
        entity.indicator.updateHistory(history);
      }
    });
  }
}
