import IPageEntity from "./reusable/types/IPageEntity";
import BaseScrapedEntity from "./reusable/types/ScrapedEntity";
import Measurement from "./reusable/types/Price";
import getIdFromPath from "../helpers/getIdFromPath";
import IIndicator from "./reusable/types/Indicator/IIndicator";
import getNumberFromPrice from "../helpers/getNumberFromPrice";

export default class ImmoScoutPageEntityExpose implements IPageEntity {
  private entityDiv: HTMLDivElement;
  // @ts-ignore
  private _indicator: IIndicator;

  constructor(entityDiv: HTMLDivElement) {
    this.entityDiv = entityDiv;
  }

  set indicator(indicator: IIndicator) {
    this._indicator = indicator;
  }

  get indicatorPlacement(): HTMLDivElement | null {
    return this.entityDiv.querySelector("#fotorama");
  }

  get scrapedEntity(): BaseScrapedEntity | undefined {
    return {
      guid: this.guid as string,
      path: window.location.pathname,
      prices: this.prices,
      isFullEntity: true,
      indicator: this.indicator,
    };
  }

  get indicator(): IIndicator {
    return this._indicator;
  }

  get guid(): string | undefined {
    return getIdFromPath(window.location.pathname);
  }

  get priceNode(): HTMLDivElement | null {
    const priceNode = this.entityDiv.querySelector(
      "#kfpriceValue"
    ) as HTMLDivElement | null;
    if (priceNode) {
      return priceNode.parentElement as HTMLDivElement;
    }
    return priceNode;
  }

  get pricesNodes(): HTMLDivElement[] {
    const priceNode = this.priceNode;
    if (priceNode) {
      return [priceNode];
    }
    return [];
  }

  private get linkNode(): HTMLAnchorElement | null {
    return null;
  }

  get prices(): Measurement[] {
    const pricesNodes = this.pricesNodes;
    return pricesNodes.reduce((acc: Measurement[], priceNode) => {
      const priceNodeValue: HTMLDivElement | null = priceNode.querySelector(
        "#kfpriceValue"
      );
      const price =
        priceNode && parseTextAsMeasurement(priceNode, String.fromCharCode(160));

      if (price) {
        acc.push(price);
      }
      return acc;
    }, []);
  }
}

function parseTextAsMeasurement(
  priceNode: HTMLDivElement,
  priceCurrencySeparator: string
): Measurement | undefined {
  const priceNodeAmount = priceNode.querySelector("#kfpriceValue");
  if (!priceNodeAmount) {
    return undefined;
  }
  const labelNode = priceNode.querySelector("#kfpriceLabel");
  const firstLine = (priceNodeAmount as HTMLDivElement).innerText.trim();
  let label = "";
  if (labelNode) {
    label = (labelNode as HTMLDivElement).innerText.trim();
  }
  const NON_BREAKING_SPACE = String.fromCharCode(8239);

  if (firstLine === "Auf Anfrage") {
    return undefined;
  }
  // const priceArr = firstLine.split(NON_BREAKING_SPACE);
  let priceArr = firstLine.split(priceCurrencySeparator);
  if (priceArr.length !== 2) {
    return undefined;
  }
  return {
    number: getNumberFromPrice(priceArr[0]),
    measure: priceArr[1],
    label,
  };
}
