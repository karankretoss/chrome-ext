import IPageEntity from "./reusable/types/IPageEntity";
import ImmoWeltPageEntityNormal from "./ImmoWeltPageEntityNormal";
import IndicatorNormal from "./reusable/types/Indicator/IndicatorNormal";
import IndicatorGrouped from "./reusable/types/Indicator/IndicatorGrouped";
import ImmoweltPageEntityExpose from "./ImmoweltPageEntityExpose";
import ImmoWeltPageEntityGrouped from "./ImmoWeltPageEntityGrouped";

export default class ImmoNetPageEntityBuilder {
  static build(entityDOMNode: HTMLDivElement): IPageEntity[] {
    const multipleListingsInOneNode =
      entityDOMNode.querySelector(".js-projectObjects");

    if (multipleListingsInOneNode) {
      const groupedEntities =
        multipleListingsInOneNode.querySelectorAll(".objectfacts");
      return Array.from(groupedEntities).map((groupedEntity) => {
        const entity = new ImmoWeltPageEntityGrouped(
          groupedEntity as HTMLDivElement
        );
        const indicatorParent = groupedEntity.querySelector("div");
        const indicator = new IndicatorGrouped(
          indicatorParent as HTMLDivElement,
          { top: "0px", right: "10px" }
        );
        entity.indicator = indicator;
        return entity;
      });
    }

    const exposePage = window.location.href.indexOf("/expose/") > -1;
    try {
      if (exposePage) {
        const exposeEntity = new ImmoweltPageEntityExpose(entityDOMNode);
        const indicatorParent = exposeEntity.indicatorPlacement;
        if (indicatorParent) {
          indicatorParent.style.position = "relative";
          exposeEntity.indicator = new IndicatorNormal(
            indicatorParent as HTMLDivElement,
            { right: "10px", margin: "0" }
          );
        }

        return [exposeEntity];
      }
    } catch (error: any) {
      console.error(`Could not create ImmoWeltPageEntityExpose due to '${error.message}'`);
      return [];
    }

    try {
      const entity = new ImmoWeltPageEntityNormal(
        entityDOMNode as HTMLDivElement
      );
      entityDOMNode.style.position = "relative";
      entity.indicator = new IndicatorNormal(
        entity.indicatorPlacement as HTMLDivElement,
        { left: "10px", zIndex: "10" }
      );
      return [entity];
    } catch (error: any) {
      console.error(`Could not create ImmoWeltPageEntityNormal due to '${error.message}'`);
      return [];
    }
  }
}
