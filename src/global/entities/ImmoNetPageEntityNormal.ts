import IPageEntity from "./reusable/types/IPageEntity";
import BaseScrapedEntity from "./reusable/types/ScrapedEntity";
import Measurement from "./reusable/types/Price";
import getIdFromPath from "../helpers/getIdFromPath";
// import IndicatorNormal from "./IndicatorNormal";
import IIndicator from "./reusable/types/Indicator/IIndicator";
import getNumberFromPrice from "../helpers/getNumberFromPrice";

export default class ImmoNetPageEntityNormal implements IPageEntity {
  private entityDiv: HTMLDivElement;
  // @ts-ignore
  _indicator: IIndicator;

  constructor(entityDiv: HTMLDivElement) {
    this.entityDiv = entityDiv;
  }

  set indicator(indicator: IIndicator) {
    this._indicator = indicator;
  }

  get indicatorPlacement(): HTMLDivElement | null {
    return this.entityDiv.querySelector(".images-wrapper");
  }

  // eslint-disable-next-line getter-return
  get scrapedEntity(): BaseScrapedEntity | undefined {
    const linkNode = this.linkNode;
    if (linkNode) {
      return {
        guid: this.guid as string,
        path: ((linkNode as any) as HTMLAnchorElement).pathname,
        prices: this.prices,
        isFullEntity: false,
        indicator: this.indicator,
      };
    }
  }

  get indicator(): IIndicator {
    return this._indicator;
  }

  get guid(): string | undefined {
    const linkNode = this.linkNode;
    if (linkNode) {
      return getIdFromPath(((linkNode as any) as HTMLAnchorElement).pathname);
    }
  }

  get pricesNodes(): HTMLDivElement[] {
    const priceNode = this.entityDiv.querySelector("[id^=selPrice]");
    if (priceNode) {
      return [priceNode as HTMLDivElement];
    }
    return [];
  }

  private get linkNode(): HTMLDivElement | null {
    return this.entityDiv.querySelector("a[id^=lnkToDetails]");
  }

  get prices(): Measurement[] {
    return this.pricesNodes.reduce((acc: Measurement[], priceNode) => {
      const price = parseTextAsPrice(priceNode, " ");
      if (price) {
        acc.push(price);
      }
      return acc;
    }, []);
  }
}

function parseTextAsPrice(
  priceNode: HTMLDivElement,
  priceCurrencySeparator: string
): Measurement | undefined {
  const priceNodeAmount = priceNode.querySelector("span.text-nowrap");
  if (!priceNodeAmount) {
    return undefined;
  }
  const labelNode = priceNode.querySelector("p");
  const firstLine = (priceNodeAmount as HTMLDivElement).innerText.trim();
  let label = "";
  if (labelNode) {
    label = (labelNode as HTMLDivElement).innerText.trim();
  }

  if (firstLine === "Auf Anfrage") {
    return undefined;
  }
  // const priceArr = firstLine.split(NON_BREAKING_SPACE);
  let priceArr = firstLine.split(priceCurrencySeparator);
  if (priceArr.length !== 2) {
    return undefined;
  }
  return {
    number: getNumberFromPrice(priceArr[0]),
    measure: priceArr[1],
    label,
  };
}
