export default interface DataObj {
    [key: string]: any
}