import IPageEntity from "./IPageEntity";
import ImmoScoutPageEntityNormal from "./ImmoScoutPageEntityNormal";
import ImmoScoutPageEntityGrouped from "./ImmoScoutPageEntityGrouped";
import IndicatorNormal from "./IndicatorNormal";
import IndicatorGrouped from "./IndicatorGrouped";
import ImmoScoutPageEntityExpose from "./ImmoScoutPageEntityExpose";
import IScrapeBuilderOptions from "./IScrapeBuilderOptions";
import type {DealType, RealestateType} from "./rotekapsel";
import {dealTypeMap, realestateMap} from "./rotekapsel";

export default class ImmoScoutPageEntityBuilder {

  static getDealAndRealestateTypesFromText(text: string): [ DealType, RealestateType ]  {
    const [ rt, dt ] = text.split(' ', 2);
    if ('Eigentumswohnung' == rt) {
      return [ 'buy', 'flat'];
    }
    const deDtlower = dt?.toLowerCase() || "";
    const deRtlower = rt?.toLowerCase() || "";

    return [ dealTypeMap[deDtlower], realestateMap[deRtlower]];
  }

  static getDealAndRealestateTypes(doc: Document): [ DealType, RealestateType ]  {
    let dealType: DealType, realestateType: RealestateType;
  
    [ dealType, realestateType ] = ImmoScoutPageEntityBuilder.getDealAndRealestateTypesFromText(doc.title);
    if (!(dealType && realestateType)) {
      try {
        const bcText = document.querySelector(".breadcrumb__item:first-child a")?.textContent || "";
        [ dealType, realestateType ] = ImmoScoutPageEntityBuilder.getDealAndRealestateTypesFromText(bcText);
      } catch (_: any) {
      }
    }
    dealType = dealType || 'buy';
    realestateType = realestateType || 'property';

    return [ dealType, realestateType ];
  }

  static handleMultipleListingsInOneNode(multipleListingsInOneNode: Element, dealType: DealType, realestateType: RealestateType): IPageEntity[] {
    const groupedEntities = multipleListingsInOneNode.querySelectorAll(
        ".grouped-listing"
    );
    return Array.from(groupedEntities).map((groupedEntity) => {
      const entity = new ImmoScoutPageEntityGrouped(
          groupedEntity as HTMLDivElement,
          dealType,
          realestateType,
      );
      const indicatorParent = groupedEntity.querySelector("div");
      const indicator = new IndicatorGrouped(
          indicatorParent as HTMLDivElement,
          { top: "10px", left: "10px", marginTop: "4px" }
      );
      entity.indicator = indicator;
      return entity;
    });
  }

  static handleExposePage(dealType: DealType, realestateType: RealestateType): IPageEntity[] {
    try {
      const exposeEntity = new ImmoScoutPageEntityExpose(
          document.querySelector("#is24-main") as HTMLDivElement,
          dealType,
          realestateType,
      );
      if (exposeEntity.isInactive) {
        return [];
      }
      const indicatorParent = exposeEntity.indicatorPlacement;
      if (indicatorParent) {
        exposeEntity.indicator = new IndicatorNormal(
          indicatorParent as HTMLDivElement,
          { left: "10px" }
        );
      }

      return [exposeEntity];
    } catch (error: any) {
      console.error(`Could not create ImmoScoutPageEntityExpose due to '${error.message}'`);
      return [];
    }
  }

  static handleListPage(entityDOMNode: HTMLDivElement, dealType: DealType, realestateType: RealestateType): IPageEntity[] {
    try {
      const entity = new ImmoScoutPageEntityNormal(
          entityDOMNode as HTMLDivElement,
          dealType,
          realestateType,
      );
      const indicator = new IndicatorNormal(
          entity.indicatorPlacement as HTMLDivElement
      );
      entity.indicator = indicator;
      return [entity];
    } catch (error: any) {
      console.error(`Could not create ImmoScoutPageEntityNormal due to '${error.message}'`);
      return [];
    }
  }

  static build({ entityDOMNode, document }: IScrapeBuilderOptions): IPageEntity[] {
    if (!entityDOMNode) {
      return [];
    }

    const [ dealType, realestateType ] = ImmoScoutPageEntityBuilder.getDealAndRealestateTypes(document);

    const multipleListingsInOneNode = entityDOMNode.querySelector(
      ".result-list-entry__grouped-listings"
    );
    if (multipleListingsInOneNode) {
      return this.handleMultipleListingsInOneNode(multipleListingsInOneNode, dealType, realestateType)
    }

    const exposePage = entityDOMNode.querySelector("#is24-main");
    if (exposePage) {
      return this.handleExposePage(dealType, realestateType)
    }

    return this.handleListPage(entityDOMNode, dealType, realestateType)
  }
}
