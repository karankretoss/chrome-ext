import IPageEntity from "./reusable/types/IPageEntity";
import ImmoNetPageEntityNormal from "./ImmoNetPageEntityNormal";
import IndicatorNormal from "./reusable/types/Indicator/IndicatorNormal";
import ImmonetPageEntityExpose from "./ImmonetPageEntityExpose";

export default class ImmoNetPageEntityBuilder {
  static build(entityDOMNode: HTMLDivElement): IPageEntity[] {
    const exposePage = window.location.href.indexOf("/angebot/") > -1;
    try {
      if (exposePage) {
        const exposeEntity = new ImmonetPageEntityExpose(entityDOMNode);
        const indicatorParent = exposeEntity.indicatorPlacement;
        const indicator = new IndicatorNormal(
          indicatorParent as HTMLDivElement,
          { right: "10px" }
        );
        exposeEntity.indicator = indicator;
        return [exposeEntity];
      }
    } catch (error: any) {
      console.error(`Could not create ImmoNetPageEntityExpose due to '${error.message}'`);
      return [];
    }

    try {
      const entity = new ImmoNetPageEntityNormal(
        entityDOMNode as HTMLDivElement
      );
      const indicator = new IndicatorNormal(
        entity.indicatorPlacement as HTMLDivElement
      );
      entity.indicator = indicator;
      return [entity];
    } catch (error: any) {
      console.error(`Could not create ImmoNetPageEntityNormal due to '${error.message}'`);
      return [];
    }
  }
}
