import IPageEntity from "./IPageEntity";
import {
  ScrapedEntityWithAdditionalProperties,
  FullPageScrapeSelector,
  FullScrapedEntity
} from "./ScrapedEntity";
import Measurement from "./Price";
import {ImmoScoutParsePriceAsPrice, ImmoscoutGetParseMeasurement} from "../helpers/ImmoScoutParsePriceNodeAsPrice";
import getIdFromPath from "../helpers/getIdFromPath";
import IIndicator from "./IIndicator";
import {DealType, RealestateType} from "./rotekapsel";
import {extractProperties, parsePropertyAddressFromAddressInExposedPage} from "../helpers/extractProperties";
import { featuresMap } from "../helpers/features.map";

export default class ImmoScoutPageEntityExpose implements IPageEntity {
  private entityDiv: HTMLDivElement;
  private dealType: DealType;
  private realestateType: RealestateType;
  // @ts-ignore
  private _indicator: IIndicator;

  private extraPropertiesSelector: FullPageScrapeSelector = {
    objectNo: [{
      selector: '.is24-scoutid',
      textParser: (elm: HTMLElement) => elm.innerText.split('|')[0].split(':')[1].trim()
    }],
    title: [{
      selector: '[data-qa="expose-title"]'
    }],
    address: [{
      selector: '[data-qa="is24-expose-address"]'
    }],
    sellerName: [{
      selector: '[data-qa="contactName"]'
    }],
    merchantName: [{
      selector: '[data-qa="company-name"]'
    }],
    rented: [{
      selector: '.is24qa-vermietet-label'
    }],
    personal: [{
      selector: '.is24qa-personenaufzug-label'
    }],
    balcony: [{
      selector: '.is24qa-balkon-terrasse-label'
    }],
    basement: [{
      selector: '.is24qa-keller-label'
    }],
    guestToilet: [{
      selector: '.is24qa-gaeste-wc-label'
    }],
    fittedKitchen: [{
      selector: '.is24qa-einbaukueche-label '
    }],
    steplessAccess: [{
      selector: '.is24qa-stufenloser-zugang-label '
    }],
    noCommissionForBuyer: [{
      selector: '.is24qa-provisionsfrei-fuer-kaufende-label'
    }],
    type: [{
      selector: 'dd.is24qa-typ'
    }],
    floor: [{
      selector: 'dd.is24qa-etagenanzahl',
      measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
    }, {
      selector: 'dd.is24qa-etage',
      measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
    }],
    livingArea:[{
      selector: 'dd.is24qa-wohnflaeche-ca',
      measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
    }] ,
    usableArea:[{
      selector: 'dd.is24qa-nutzflaeche-ca',
      measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
    }] ,
    approxLandArea:[{
      selector: 'dd.is24qa-grundstueck-ca',
      measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
    }] ,
    vacantFrom:[{
      selector: 'dd.is24qa-bezugsfrei-ab'
    }] ,
    room:[{
      selector: 'dd.is24qa-zimmer',
      measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
    }] ,
    bedroom:[{
      selector: 'dd.is24qa-schlafzimmer',
      measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
    }] ,
    bathroom:[{
      selector: 'dd.is24qa-badezimmer',
      measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
    }] ,
    parkingSpace:[{
      selector: 'dd.is24qa-garage-stellplatz'
    }] ,
    // Costs
    baseRent:[{
      selector: 'dd.is24qa-kaltmiete',
      measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
    }] ,
    totalRent: [{
      selector: '.is24qa-warmmiete-main',
      measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
    }],
    additionalCosts:[{
      selector: 'dd.is24qa-nebenkosten',
      measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
    }] ,
    heatingCost:[{
      selector: 'dd.is24qa-heizkosten',
      measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
    }] ,
    fullRent:[{
      selector: 'dd.is24qa-gesamtmiete',
      measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm, " "),
    }],
    provision: [{
      selector: 'dd.is24qa-provision'
    }],
    provisionNote: [{
      selector: '.is24qa-provision-note'
    }],
    depositOrCooperativeShare: [{
      selector: '.is24qa-kaution-o-genossenschaftsanteile'
    }],
    forRentGarage: [{
      selector: 'dd.is24qa-miete-fuer-garagestellplatz'
    }],
    rentalIncomePerMonthShouldBe: [{
      selector: 'dd.is24qa-mieteinnahmen-pro-monat',
      measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm),
    }],
    tenantCommission: [{
      selector: 'dd.is24qa-provision'
    }],
    garagePurchasePrice: [{
      selector: 'dd.is24qa-garage-stellplatz-kaufpreis',
      measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm),
    }],
    purchasePrice: [{
      selector: '.is24qa-kaufpreis',
      measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm),
    }],
    otherPayments: [{
      selector: '.is24qa-hausgeld',
      measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm),
    }],
    // Building fabric & energy certificate
    constructionYear:[{
      selector: 'dd.is24qa-baujahr',
      measureParser: (elm: HTMLElement | string | undefined) => ImmoscoutGetParseMeasurement(elm),
    }] ,
    constructionYearByEnergyCertificate: [{
      selector: '.is24qa-baujahr-laut-energieausweis'
    }],
    constructionPhase: [{
      selector: 'dd.is24qa-bauphase '
    }],
    thinkProtectiveObject: [{
      selector: 'dd.is24qa-denkmalschutzobjekt'
    }],
    essentialEnergyCarriers: [{
      selector: '.is24qa-wesentliche-energietraeger'
    }],
    energyCertificate: [{
      selector: '.is24qa-energieausweis'
    }],
    energyCertificateType: [{
      selector: '.is24qa-energieausweistyp'
    }],
    finalEnergyConsumption: [{
      selector: '.is24qa-endenergieverbrauch'
    }],
    modernization: [{
      selector: 'dd.is24qa-modernisierung-sanierung '
    }],
    furnishing:[{
      selector: 'dd.is24qa-qualitaet-der-ausstattung'
    }] ,
    heatingType:[{
      selector: 'dd.is24qa-heizungsart'
    }] ,
    objectState: [{
      selector: 'dd.is24qa-objektzustand'
    }],
    furnishingDetail: [{
      selector: 'dd.is24qa-ausstattung'
    }],
    miscellaneous: [{
      selector: '.is24qa-sonstiges'
    }],
    genericFeatures: [{
      selector: '.criteriagroup.boolean-listing span.palm-hide'
    }]
  }

  constructor(entityDiv: HTMLDivElement, dealType: DealType, realestateType: RealestateType) {
    this.entityDiv = entityDiv;
    this.dealType = dealType;
    this.realestateType = realestateType;
  }

  get isInactive(): boolean {
    const msgBox: HTMLDivElement | null = this.entityDiv.querySelector("#is24-main .status-message.status-warning h3");
    if (msgBox) {
      const messageText = msgBox.textContent;
      if (messageText === 'Angebot wurde deaktiviert' || messageText === 'Angebot nicht gefunden') {
        return true;
      }
    }
    return false;
  }

  set indicator(indicator: IIndicator) {
    this._indicator = indicator;
  }

  get indicatorPlacement(): HTMLDivElement | null {
    const result = this.entityDiv.querySelector(".gallery-picture-container") as HTMLDivElement
    if (result !== null)
      return result
    return this.entityDiv.querySelector('.is24-expose-gallery-box')
  }

  get extraProperties(): ScrapedEntityWithAdditionalProperties {
    const clonedDiv = this.entityDiv.cloneNode(true) as HTMLElement;
    const result = extractProperties(clonedDiv, this.extraPropertiesSelector);

    // additional handler for special properties
    if (result.merchantName === undefined) {
      const isPrivateCompanyNode = this.entityDiv.querySelector('.brandLogoPrivate_dnns4')
      if (isPrivateCompanyNode) {
        result.merchantName = 'Private';
      }
    }
    if (this.extraPropertiesSelector.genericFeatures !== undefined) {
      result.genericFeatures = Array.from(this.entityDiv.querySelectorAll(this.extraPropertiesSelector.genericFeatures[0].selector))
          .map(node => {
            const val = (node as HTMLElement).innerText || '';
            return featuresMap[val] || val;
          })
          .filter(feature => !['...', ''].includes(feature));
    }
    if (result.sellerName || result.merchantName) {
      result.seller = {
        isMerchant: 'Private' !== result.merchantName,
        sellerName: result.sellerName,
        merchantName: 'Private' === result.merchantName ? '' : result.merchantName,
      }
      delete result.sellerName;
      delete result.merchantName;
    }

    result.propertyAddress = parsePropertyAddressFromAddressInExposedPage(result.address);

    return result
  }

  get scrapedEntity(): FullScrapedEntity | undefined {
    const extraProperties = this.extraProperties
    return {
      ...extraProperties,
      guid: this.guid as string,
      path: window.location.pathname,
      prices: this.prices,
      dealType: this.dealType,
      realestateType: this.realestateType,
      isFullEntity: true,
      indicator: this.indicator,
    };
  }

  get indicator(): IIndicator {
    return this._indicator;
  }

  get objectNo(): string | undefined {
    try {
      const elm = this.entityDiv.querySelector('.is24-scoutid') as HTMLElement
      return elm.innerText.split('|')[0].split(':')[1].trim()
    } catch (_) {
      return undefined
    }

  }
  get guid(): string | undefined {
    return getIdFromPath(window.location.pathname);
  }

  get priceNode(): NodeListOf<HTMLDivElement> {
    return this.entityDiv.querySelectorAll(".is24-preis-section");
  }

  get pricesNodes(): HTMLDivElement[] {
    return Array.from(this.priceNode) || [];
  }

  private get linkNode(): HTMLAnchorElement | null {
    return null;
  }

  get prices(): Measurement[] {
    const pricesNodes = this.pricesNodes;
    return pricesNodes.reduce((acc: Measurement[], priceNode) => {
      const priceNodeValue: HTMLDivElement | null = priceNode.querySelector(
          ".is24-value"
      );
      const price =
          priceNodeValue &&
          ImmoScoutParsePriceAsPrice(priceNodeValue.innerText.trim(), " ");
      const label = priceNode.querySelector(".is24-label") as
          | HTMLDivElement
          | undefined;
      if (price && label) {
        acc.push({
          ...price,
          label: label.innerText.trim(),
        });
      }
      return acc;
    }, []);
  }
}
