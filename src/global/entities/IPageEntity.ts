import BaseScrapedEntity, {FullScrapedEntity} from "./ScrapedEntity";
import IIndicator from "./IIndicator";
import Measurement from "./Price";

export default interface IPageEntity {
  scrapedEntity: BaseScrapedEntity | FullScrapedEntity | undefined;
  pricesNodes: HTMLDivElement[];
  prices: Measurement[];
  indicatorPlacement: HTMLDivElement | null;
  guid: string | undefined;
  indicator: IIndicator;
}
