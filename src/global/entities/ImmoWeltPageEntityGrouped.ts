import getIdFromPath from "../helpers/getIdFromPath";
import { ImmoScoutParsePriceAsPrice } from "../helpers/ImmoScoutParsePriceNodeAsPrice";
import IPageEntity from "./reusable/types/IPageEntity";
import Measurement from "./reusable/types/Price";
import BaseScrapedEntity from "./reusable/types/ScrapedEntity";
import IIndicator from "./reusable/types/Indicator/IIndicator";
import getNumberFromPrice from "../helpers/getNumberFromPrice";

export default class ImmoWeltPageEntityGrouped implements IPageEntity {
  private entityDiv: HTMLDivElement;
  // @ts-ignore
  private _indicator: IIndicator;

  constructor(entityDiv: HTMLDivElement) {
    this.entityDiv = entityDiv;
  }

  set indicator(indicator: IIndicator) {
    this._indicator = indicator;
  }

  get indicatorPlacement(): HTMLDivElement | null {
    return this.entityDiv.querySelector(".objectfact strong");
  }

  get scrapedEntity(): BaseScrapedEntity | undefined {
    const linkNode = this.linkNode;
    if (linkNode) {
      return {
        guid: this.guid as string,
        path: ((linkNode as any) as HTMLAnchorElement).pathname,
        prices: this.prices,
        isFullEntity: false,
        indicator: this.indicator,
      };
    }
  }

  get indicator(): IIndicator {
    return this._indicator;
  }

  // eslint-disable-next-line getter-return
  get guid(): string | undefined {
    const linkNode = this.linkNode;
    if (linkNode) {
      return getIdFromPath(((linkNode as any) as HTMLAnchorElement).pathname);
    }
  }

  get pricesNodes(): HTMLDivElement[] {
    const dataSpans = this.entityDiv.querySelectorAll(".objectfact strong");
    if (dataSpans.length > 0) {
      return [dataSpans[0] as HTMLDivElement];
    }
    return [];
  }

  get prices(): Measurement[] {
    const pricesNodes: HTMLDivElement[] = this.pricesNodes;
    return pricesNodes.reduce((acc: Measurement[], priceNode: HTMLDivElement) => {
      if (priceNode) {
        const price = ImmoScoutParsePriceAsPrice(priceNode.innerText, " ");
        if (price) {
          acc.push(price);
        }
      }
      return acc;
    }, []);
  }

  private get linkNode(): HTMLAnchorElement | null {
    return this.entityDiv.querySelector("a");
  }
}

export function parsePriceStringAsPrice(
  price: string,
  priceCurrencySeparator: string
): Measurement | undefined {
  const NON_BREAKING_SPACE = String.fromCharCode(8239);
  if (price.indexOf("auf Anfrage") > -1) {
    return undefined;
  }
  // const priceArr = firstLine.split(NON_BREAKING_SPACE);
  const priceArr = price.split(priceCurrencySeparator);
  if (priceArr.length !== 2) {
    return undefined;
  }
  return {
    number: getNumberFromPrice(priceArr[0]),
    measure: priceArr[1],
    label: "",
  };
}
