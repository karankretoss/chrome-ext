import BaseScrapedEntity from "./ScrapedEntity";
import ImmoScoutPageEntityBuilder from "./ImmoScoutPageEntityBuilder";
import IPageEntity from "./IPageEntity";
import IHistoricData from "./IHistoricData";
import {ExtensionCustomEvent} from "./ExtensionCustomEvent";
import {MAX_RETRY_GET_COUNTRY_AT_EXPOSE_PAGE} from "../constants";
import Favorite from "./Favorite";
import {isValueOfEntitiesEqual} from "../helpers";

declare global {
  interface Window {
    IS24: any
  }
}

export default class ImmoScout24EntityScraper {
  private entities: IPageEntity[] | undefined;

  getIS24 = async () => {
    document.dispatchEvent(new CustomEvent(ExtensionCustomEvent.NotifyFromContentToMainWorldForIS24));
    return new Promise((resolve, reject) => {
      document.addEventListener(ExtensionCustomEvent.SendIS24FromMainWorldCToContent,  (e: any) => {
        if (e.detail && e.detail.payload !== undefined) {
          const {IS24, counter} = e.detail.payload
          try {
            window.IS24 = JSON.parse(IS24)
          } catch (_) {}
          if (IS24 || counter === MAX_RETRY_GET_COUNTRY_AT_EXPOSE_PAGE) {
            document.dispatchEvent(new CustomEvent(ExtensionCustomEvent.ReceiveS24FromMainWorldCToContent))
            resolve(true)
          }
        }
      })
    })
  }

  async initializeListPage(): Promise<void> {
    await this.waitForPageReady();
    await this.getIS24()

    const entitiesDOMNodes: NodeListOf<HTMLDivElement> = document.querySelectorAll(
        "#resultListItems li.result-list__listing"
    ) || [];

    this.entities = Array.from(entitiesDOMNodes)
        .map((entityDOMNode: HTMLDivElement) =>
            ImmoScoutPageEntityBuilder.build({ entityDOMNode, document })
        )
        .reduce((acc: IPageEntity[], entities: IPageEntity[]) => {
          entities.forEach((entity) => {
            acc.push(entity)
          });
          return acc;
        }, [] as IPageEntity[]) || [];
  }

  async initializeExposePage(): Promise<void> {
    await this.getIS24()
    const entityDOMNode = document.querySelector(".viewport") as HTMLDivElement;
    this.entities = ImmoScoutPageEntityBuilder.build({ entityDOMNode, document });
  }

  destroy() {
    this.entities?.forEach((entity) => {
      entity.indicator.element.remove();
    });
  }

  async waitForPageReady() {
    return await new Promise((resolve, reject) => {
      let isActive = true;
      let interval = setInterval(() => {
        const entitiesNodes = document.querySelectorAll(".result-list-entry");
        if (entitiesNodes.length > 3 && isActive) {
          isActive = true;
          clearInterval(interval);
          resolve(true);
        }
      }, 1000);
      setTimeout(() => {
        if (isActive) {
          isActive = true;
          clearInterval(interval);
          resolve(true);
        }
      }, 6000);
    });
  }

  async getPageEntities(): Promise<BaseScrapedEntity[]> {
    this.entities = this.entities || [];
    return this.entities
        .map((entity) => {
          if (!entity.scrapedEntity) {
            return undefined;
          }
          const { extraPropertiesViaDataObj, ...rest } = entity.scrapedEntity as any
          return rest
        })
        .filter((scrapedEntity) => !!scrapedEntity) as BaseScrapedEntity[];
  }

  async getFavorites(): Promise<Favorite[]> {
    return this.entities?.map((entity) => {
      if (!entity.scrapedEntity) {
        return undefined;
      }
      const {extraPropertiesViaDataObj, ...rest} = entity.scrapedEntity as any;
      const fields = Object.keys(extraPropertiesViaDataObj || {})
      .filter(key => !isValueOfEntitiesEqual(extraPropertiesViaDataObj[key], rest[key]));

      if (fields.length) {
        return ({
          id: rest.guid,
          fields,
        }) as Favorite;
      }
      return undefined;
    })
      .filter((favorite) => !!favorite) as Favorite[];
  }

  updateIndicators(historicData: IHistoricData[]): void {
    if (!(historicData && historicData.length)) {
      return;
    }
    historicData.forEach((history: IHistoricData) => {
      const entity = this.entities?.find(
          (entity) => entity.guid === history.guid
      );
      if (entity) {
        entity.indicator.updateHistory(history);
      }
    });
  }
}
