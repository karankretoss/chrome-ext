export enum ExtensionCustomEvent {
    SendIS24FromMainWorldCToContent='SendIS24FromMainWorldCToContent',
    NotifyFromContentToMainWorldForIS24='NotifyFromContentToMainWorldForIS24',
    ReceiveS24FromMainWorldCToContent='ReceiveIS24FromMainWorldCToContent'
}