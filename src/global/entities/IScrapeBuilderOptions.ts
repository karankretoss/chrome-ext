export default interface IScrapeBuilderOptions {
    entityDOMNode: HTMLDivElement | undefined;
    document: Document;
}

export interface IScrapeBuilderOptionsV2 {
    data: any,
    document: Document;
}
