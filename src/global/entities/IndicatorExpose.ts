import IIndicator from "./IIndicator";
import SPINNER_SVG from "../UI/svg/spinner";
import IndicatorStatus from "./IndicatorStatus";
import IHistoricData from "./IHistoricData";
import TRENDING_FLAT_SVG from "../UI/svg/trendingFlat";
import TRENDING_DOWN_SVG from "../UI/svg/trendingDown";
import TRENDING_UP_SVG from "../UI/svg/trendingUp";
import putMoreInfoPopover from "../UI/putMoreInfoPopover";

export default class IndicatorExpose implements IIndicator {
  private _element: HTMLDivElement;
  // @ts-ignore
  private history: IHistoricData;
  constructor(positionElement: HTMLDivElement) {
    this._element = document.createElement("div");
    this._element.style.zIndex = "50";
    this._element.style.position = "absolute";
    this._element.style.cursor = "pointer";
    this._element.style.margin = "4px 0";
    this._element.style.display = "flex";
    this._element.style.justifyContent = "center";
    this._element.style.alignItems = "center";
    this._element.innerHTML = SPINNER_SVG;

    this._element.style.left = "10px";
    positionElement.prepend(this._element);
  }

  get element(): HTMLDivElement {
    return this._element;
  }

  updateHistory(history: IHistoricData): void {
    this.history = history;

    const priceDiffStatus = this.getPriceDiffStatus(this.history);
    switch (priceDiffStatus) {
      case IndicatorStatus.SAME: {
        this.element.innerHTML = `
        <div style="width: 56px; height: 40px; border-radius: 20px 0px 0px 20px; box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.05); display: flex; justify-content: center; align-items: center; background: linear-gradient(180deg, #A388DD 0%, #8F6ED5 100%);">
          ${TRENDING_FLAT_SVG({size: 24})}
        </div>
        <div style="width: 74px; height: 36px; background: #FFFFFF; border: 1px solid #E1E5EA; box-sizing: border-box; box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.05); border-radius: 0px 20px 20px 0px; display: flex; justify-content: center; align-items: center;">
          45 days
        </div>
        `
        // this.element.innerHTML = TRENDING_FLAT_SVG;
        break;
      }
      case IndicatorStatus.DOWN: {
        this.element.innerHTML = TRENDING_DOWN_SVG({});
        break;
      }
      case IndicatorStatus.UP: {
        this.element.innerHTML = TRENDING_UP_SVG({});
        break;
      }
    }
    putMoreInfoPopover({
      domElement: this.element,
      historicData: this.history,
    });
  }

  private getPriceDiffStatus(entityHistory: IHistoricData): IndicatorStatus {
    const changesWithActualPrice = entityHistory.changes.filter(
      (change) => change.price.currency !== "?"
    );
    if (changesWithActualPrice.length < 2) {
      return IndicatorStatus.SAME;
    }
    if (
      changesWithActualPrice[changesWithActualPrice.length - 1].price.amount >
      changesWithActualPrice[changesWithActualPrice.length - 2].price.amount
    ) {
      return IndicatorStatus.UP;
    }
    if (
      changesWithActualPrice[changesWithActualPrice.length - 1].price.amount <
      changesWithActualPrice[changesWithActualPrice.length - 2].price.amount
    ) {
      return IndicatorStatus.DOWN;
    }
    return IndicatorStatus.SAME;
  }

  setStatus(indicatorStatus: IndicatorStatus): void {}
}
