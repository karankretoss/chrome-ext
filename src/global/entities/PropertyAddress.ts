export interface PropertyAddress {
    houseNumber?: string;
    street?: string;
    city?: string;
    region?: string;
    quarter?: string;
    zipCode?: string;
    country?: string;
}