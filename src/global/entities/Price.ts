export default interface Measurement {
  number: number;
  measure: string;
  label: string;
}
