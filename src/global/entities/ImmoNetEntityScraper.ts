import BaseScrapedEntity from "./reusable/types/ScrapedEntity";
import ImmoNetPageEntityBuilder from "./ImmoNetPageEntityBuilder";
import IPageEntity from "./reusable/types/IPageEntity";
import IHistoricData from "./reusable/types/IHistoricData";

export default class ImmoNetEntityScraper {
  // @ts-ignore
  private entities: IPageEntity[];

  async initializeListPage(): Promise<void> {
    const entitiesDOMNodes: NodeListOf<HTMLDivElement> = document.querySelectorAll(
        "#result-list-stage div.item"
    );

    this.entities = Array.from(entitiesDOMNodes)
        .map((entitiesDOMNode: HTMLDivElement) =>
            ImmoNetPageEntityBuilder.build(entitiesDOMNode)
        )
        .reduce((acc: IPageEntity[], entities: IPageEntity[]) => {
          entities.forEach((entity) => acc.push(entity));
          return acc;
        }, [] as IPageEntity[]);
  }

  destroy() {
    this.entities.forEach((entity) => {
      if (entity.indicator)
        entity.indicator.element.remove();
    });
  }

  async initializeExposePage(): Promise<void> {
    this.entities = ImmoNetPageEntityBuilder.build(
        (document.querySelector("body") as any) as HTMLDivElement
    );
  }

  async waitForPageReady() {
    return await new Promise((resolve, reject) => {
      let interval = setInterval(() => {
        const entitiesNodes = document.querySelectorAll(".result-list-entry");
        if (entitiesNodes.length > 3) {
          clearInterval(interval);
          resolve(true);
        }
      }, 1000);
    });
  }

  async getPageEntities(): Promise<BaseScrapedEntity[]> {
    return this.entities
        .map((entity) => entity.scrapedEntity)
        .filter((scrapedEntity) => scrapedEntity) as BaseScrapedEntity[];
  }

  updateIndicators(historicData: IHistoricData[]): void {
    if (!(historicData && historicData.length)) {
      return;
    }
    historicData.forEach((history: IHistoricData) => {
      const entity = this.entities.find(
          (entity) => entity.guid === history.guid
      );
      if (entity && entity.indicator) {
        entity.indicator.updateHistory(history);
      }
    });

  }
}
