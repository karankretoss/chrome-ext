import {ScrapedEntityWithAdditionalProperties} from "../types/ScrapedEntity";
import {parsePropertyAddressFromAddressInExposedPage} from "../../../helpers/extractProperties";

export function debounce<F extends Function, T extends unknown[]>(func: F, timeout: number = 300): (this: any, ...args: T) => void {
    let timer: ReturnType<typeof setTimeout>;

    return function (this: any, ...args: T): void {
        clearTimeout(timer);
        timer = setTimeout(() => { func.apply(this, args); }, timeout);
    };
}

export const getZipCodeFromExtraProperties = (extraProperties: ScrapedEntityWithAdditionalProperties): string | undefined => {
    if (extraProperties.address) {
        const parsedAddress = parsePropertyAddressFromAddressInExposedPage(extraProperties.address);
        if (parsedAddress && parsedAddress.zipCode) {
            return parsedAddress.zipCode
        }
    }

    return undefined


}

export const getLastArrayElement = (arr: any[]) => {
    return arr[arr.length - 1];
}

export const wait = (ms: number) => new Promise(r => setTimeout(r, ms));