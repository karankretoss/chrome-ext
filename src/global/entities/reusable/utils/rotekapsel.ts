export type DealType = 'buy' | 'rent' | 'unknown';
export type RealestateType = 'flat' | 'geriatricCare' | 'hall' | 'house' | 'property' | 'industrialLand' | 'investment' | 'land' | 'liquidation' | 'office' | 'other' | 'parking' | 'restaurant_hotel' | 'seniorLiving' | 'sharedRoom' | 'shop' | 'specialBusiness' | 'temporaryLiving' | 'trade_site';
export const dealTypeMap: Record<string, DealType> = {
  'mieten': 'rent',
  'kaufen': 'buy',
};
export const realestateMap: Record<string, RealestateType> = {
  'wohnung': 'flat',
  'haus': 'house',
  'gastronomy': 'restaurant_hotel',
  'industry': 'hall',
  'trade_site': 'industrialLand',
  'premises': 'land',
  'grundstück': 'property',
  'buero': 'office',
  'büros/praxen': 'office',
  'büros': 'office',
  'office': 'office',
  'laden': 'shop',
};
