import Favorite from "./Favorite"
import ScrapedEntity from "./ScrapedEntity"

export interface InitializeListPageProps {
    entitiesDOMNodes: NodeListOf<HTMLDivElement>,
    isProjectAds?: boolean
}

export interface PayLoadSendToServer {
    source: "IS24",
    entities: ScrapedEntity[],
    favorites?: Favorite[]
}