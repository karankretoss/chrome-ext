export default interface IHistoricData {
  guid: string;
  changes: {
    price: { amount: number; currency: string };
    date: string;
  }[];
}
