import IndicatorStatus from "./IndicatorStatus";
import IHistoricData from "../IHistoricData";
import TRENDING_FLAT_SVG from "../../../../UI/svg/trendingFlat";
import TRENDING_DOWN_SVG from "../../../../UI/svg/trendingDown";
import TRENDING_UP_SVG from "../../../../UI/svg/trendingUp";
import Indicator from "./Indicator";

export default class IndicatorNormal extends Indicator{
  constructor(
    positionElement: HTMLDivElement,
    styleOverride?: { [key: string]: string }
  ) {
    super(positionElement);
    this._element.style.pointerEvents = "all";
    this._element.style.margin = "4px 0";
    this._element.style.justifyContent = "center";
    this._element.style.alignItems = "center";

    if (styleOverride) {
      Object.entries(styleOverride).map(([key, value]) => {
        this._element.style[key as any] = value;
      });
    }
  }

  updateHistoryHelper(history: IHistoricData) {
    const priceDiffStatus = this.getPriceDiffStatus(this.history);

    switch (priceDiffStatus) {
      case IndicatorStatus.SAME: {
        this.element.innerHTML = `
        <div style="width: 35px; height: 25px; border-radius: 20px 0px 0px 20px; box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.05); display: flex; justify-content: center; align-items: center; background: linear-gradient(180deg, #A388DD 0%, #8F6ED5 100%);">
          ${TRENDING_FLAT_SVG({ size: 24 })}
        </div>
        <div style="width: 46px; height: 23px; font-size: 14px; background: #FFFFFF; border: 1px solid #E1E5EA; box-sizing: border-box; box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.05); border-radius: 0px 20px 20px 0px; display: flex; justify-content: center; align-items: center;">
          ${this.pillText}
        </div>
        `;
        // this.element.innerHTML = TRENDING_FLAT_SVG;
        break;
      }
      case IndicatorStatus.DOWN: {
        this.element.innerHTML = `
        <div style="width: 35px; height: 25px; border-radius: 20px 0px 0px 20px; box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.05); display: flex; justify-content: center; align-items: center; background: linear-gradient(180deg, #66C5FF 0%, #33B1FF 100%);">
          ${TRENDING_DOWN_SVG({ size: 24 })}
        </div>
        <div style="width: 46px; height: 23px; font-size: 14px; background: #FFFFFF; border: 1px solid #E1E5EA; box-sizing: border-box; box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.05); border-radius: 0px 20px 20px 0px; display: flex; justify-content: center; align-items: center;">
          ${this.pillText}
        </div>
        `;
        break;
      }
      case IndicatorStatus.UP: {
        this.element.innerHTML = `
        <div style="width: 35px; height: 25px; border-radius: 20px 0px 0px 20px; box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.05); display: flex; justify-content: center; align-items: center; background: linear-gradient(180deg, #FF974D 0%, #FF832B 100%);">
          ${TRENDING_UP_SVG({ size: 24 })}
        </div>
        <div style="width: 46px; height: 23px; font-size: 14px; background: #FFFFFF; border: 1px solid #E1E5EA; box-sizing: border-box; box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.05); border-radius: 0px 20px 20px 0px; display: flex; justify-content: center; align-items: center;">
          ${this.pillText}
        </div>
        `;
        break;
      }
    }
  }
}
