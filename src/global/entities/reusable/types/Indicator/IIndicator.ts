import IndicatorStatus from "./IndicatorStatus";
import IHistoricData from "../IHistoricData";

export default interface IIndicator {
  element: HTMLDivElement;
  setStatus(indicatorStatus: IndicatorStatus): void;
  updateHistory(history: IHistoricData): void;
}
