enum IndicatorStatus {
  "IN_SYNC",
  "NEW",
  "UP",
  "SAME",
  "DOWN",
}

export default IndicatorStatus;
