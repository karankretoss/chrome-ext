import Indicator from "./Indicator";
import IHistoricData from "../IHistoricData";
import IndicatorStatus from "./IndicatorStatus";
import TRENDING_FLAT_SVG from "../../../../UI/svg/trendingFlat";
import TRENDING_DOWN_SVG from "../../../../UI/svg/trendingDown";
import TRENDING_UP_SVG from "../../../../UI/svg/trendingUp";

export default class IndicatorExpose extends Indicator {
    constructor(positionElement: HTMLDivElement) {
        super(positionElement);

        this._element.style.margin = "4px 0";
        this._element.style.justifyContent = "center";
        this._element.style.alignItems = "center";
        this._element.style.left = "10px";
    }

    updateHistoryHelper(history: IHistoricData) {
        const priceDiffStatus = this.getPriceDiffStatus(this.history);
        switch (priceDiffStatus) {
            case IndicatorStatus.SAME: {
                this.element.innerHTML = `
        <div style="width: 56px; height: 40px; border-radius: 20px 0px 0px 20px; box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.05); display: flex; justify-content: center; align-items: center; background: linear-gradient(180deg, #A388DD 0%, #8F6ED5 100%);">
          ${TRENDING_FLAT_SVG({size: 24})}
        </div>
        <div style="width: 74px; height: 36px; background: #FFFFFF; border: 1px solid #E1E5EA; box-sizing: border-box; box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.05); border-radius: 0px 20px 20px 0px; display: flex; justify-content: center; align-items: center;">
          45 days
        </div>
        `
                // this.element.innerHTML = TRENDING_FLAT_SVG;
                break;
            }
            case IndicatorStatus.DOWN: {
                this.element.innerHTML = TRENDING_DOWN_SVG({});
                break;
            }
            case IndicatorStatus.UP: {
                this.element.innerHTML = TRENDING_UP_SVG({});
                break;
            }
        }
    }
}
