import IIndicator from './IIndicator';
import IndicatorStatus from './IndicatorStatus';
import IHistoricData from '../IHistoricData';
import putMoreInfoPopover from '../../../../UI/putMoreInfoPopover';
import { className } from '../../../../constants';
import PriceIndicatorLoader from '../../../../../components/PriceIndicatorLoader';

const DAY_IN_MS = 1000 * 60 * 60 * 24;
export default class Indicator implements IIndicator {
  protected _element: HTMLDivElement;
  // @ts-ignore
  protected history: IHistoricData;
  protected tooltipExist = false;
  getExistingIndicator = (positionElement: HTMLDivElement) =>
    positionElement.querySelector(`.${className.Indicator}`);
  constructor(
    positionElement: HTMLDivElement,
    styleOverride?: { [key: string]: string }
  ) {
    if (this.getExistingIndicator(positionElement)) {
      this.tooltipExist = true;
      this._element = this.getExistingIndicator(
        positionElement
      ) as HTMLDivElement;
      return;
    }

    this._element = document.createElement('div');
    this._element.className = className.Indicator;
    this._element.style.zIndex = '50';
    this._element.style.position = 'absolute';
    this._element.style.cursor = 'pointer';
    this._element.style.display = 'flex';
    const svgLoader = PriceIndicatorLoader();
    this._element.appendChild(svgLoader);

    if (styleOverride) {
      Object.entries(styleOverride).map(([key, value]) => {
        this._element.style[key as any] = value;
      });
    }
    positionElement.prepend(this._element);
  }

  private dateIsInDays(date: Date, days: number): boolean {
    if (!(days > 0 || Number.isInteger(days))) {
      throw new Error(`'days' parameter must be positive non-zero integer.`);
    }
    const diffMS = new Date().getTime() - date.getTime();
    return diffMS < days * DAY_IN_MS;
  }

  get pillText(): string {
    const initialDate =
      this.history.changes.length > 0
        ? new Date(this.history.changes[0].date)
        : new Date();
    const now = new Date();

    const daysPassed = Math.floor(
      (now.getTime() - initialDate.getTime()) / DAY_IN_MS
    );
    return daysPassed === 0
      ? 'Neu'
      : daysPassed > 99
      ? '+99 T'
      : daysPassed + ' T';
  }

  updateHistoryHelper(history: IHistoricData) {}

  get element(): HTMLDivElement {
    return this._element;
  }

  getChangesWithActualPrice(entityHistory: IHistoricData) {
    return entityHistory.changes.filter(
      (change) =>
        change?.price?.currency !== '?' && !isNaN(change?.price?.amount)
    );
  }

  shouldDisplayRedDot(entityHistory: IHistoricData): Boolean {
    const changesWithActualPrice = [
      ...this.getChangesWithActualPrice(entityHistory),
    ];
    changesWithActualPrice.sort((v) => new Date(v.date).getTime());
    if (changesWithActualPrice.length > 1) {
      const lastElement = changesWithActualPrice.pop();
      if (lastElement?.date) {
        return this.dateIsInDays(new Date(lastElement.date), 3);
      }
    }
    return false;
  }

  protected getPriceDiffStatus(entityHistory: IHistoricData): IndicatorStatus {
    const changesWithActualPrice =
      this.getChangesWithActualPrice(entityHistory);

    if (changesWithActualPrice.length < 2) {
      return IndicatorStatus.SAME;
    }
    if (
      changesWithActualPrice[changesWithActualPrice.length - 1].price.amount >
      changesWithActualPrice[changesWithActualPrice.length - 2].price.amount
    ) {
      return IndicatorStatus.UP;
    }
    if (
      changesWithActualPrice[changesWithActualPrice.length - 1].price.amount <
      changesWithActualPrice[changesWithActualPrice.length - 2].price.amount
    ) {
      return IndicatorStatus.DOWN;
    }
    return IndicatorStatus.SAME;
  }

  updateHistory(history: IHistoricData) {
    this.history = history;
    // ReactDOM.unmountComponentAtNode(this._element);
    var container = this._element.querySelector('div');

    // Remove the container from the DOM
    if (container) {
      this._element.removeChild(container);
    }

    this.updateHistoryHelper(history);
    if (this.shouldDisplayRedDot(history)) {
      this.element.innerHTML += `<div class="${className.IndicatorDot}"></div>`;
    }

    if (!this.tooltipExist)
      putMoreInfoPopover({
        domElement: this.element,
        historicData: this.history,
      });
  }
  setStatus(indicatorStatus: IndicatorStatus): void {}
}
