export enum Source {
    none='',
    immoScout24='IS24',
    immoScout24At='ISAT',
    kleinanzeigen='KLAZ',
    willHaben='WILH',
    immonet='IMNT',
    immowelt='IMWT'
}