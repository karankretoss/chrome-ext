export enum ExtensionCustomEvent {
    SendAdsDataFromMainWorldCToContent='SendAdsDataFromMainWorldCToContent',
    NotifyFromContentToMainWorldForAdsData='NotifyFromContentToMainWorldForAdsData',
    ReceiveAdsDataFromMainWorldCToContent='ReceiveAdsDataFromMainWorldCToContent'
}