import { DealType, RealestateType } from "../../rotekapsel";

export default interface IScrapeBuilderOptions {
    entityDOMNode: HTMLDivElement | undefined;
    isProjectAds?: boolean;
    dealType?: DealType;
    realestateType?: RealestateType;
}

export interface IScrapeBuilderOptionsV2 {
    data: any,
    document: Document;
}
