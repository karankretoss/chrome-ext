import Measurement from "./Price";
import IIndicator from "./Indicator/IIndicator";
import type { DealType, RealestateType } from "../utils/rotekapsel";
import {PropertyAddress} from "./PropertyAddress";

export default interface BaseScrapedEntity {
  guid: string;
  path: string;
  prices: Measurement[];
  dealType?: DealType;
  isFullEntity: boolean;
  realestateType?: RealestateType;
  indicator: IIndicator;
}

export interface ScrapedEntityWithAdditionalProperties {
  // fundamental properties
  objectNo: string | undefined,
  title: string | undefined,
  address: string | undefined,
  zipCode: string | undefined,
  propertyAddress: PropertyAddress | undefined,
  sellerName: string | undefined,
  merchantName: string | undefined,
  rented: string | undefined,
  // Chip
  balcony: string | undefined,
  basement: string | undefined,
  personal: string | undefined,
  guestToilet: string | undefined,
  steplessAccess: string | undefined,
  fittedKitchen: string | undefined,
  noCommissionForBuyer: string | undefined,
  type: string | undefined,
  floor: Measurement | undefined,
  livingArea: Measurement | undefined,
  usableArea: string | undefined,
  approxLandArea: string | undefined,
  vacantFrom: string | undefined,
  room: Measurement | undefined,
  bedroom: Measurement | undefined,
  bathroom: Measurement | undefined,
  parkingSpace: string | undefined,
  // Costs
  baseRent: Measurement | undefined,
  totalRent: Measurement | undefined,
  additionalCosts: Measurement | undefined,
  heatingCost: string | undefined,
  provision: string | undefined,
  fullRent: Measurement | undefined,
  depositOrCooperativeShare: string | undefined,
  forRentGarage: string | undefined,
  provisionNote: string | undefined,
  rentalIncomePerMonthShouldBe: string | undefined,
  tenantCommission: string | undefined,
  garagePurchasePrice: string | undefined,
  purchasePrice: Measurement | undefined,
  price: Measurement | undefined,
  otherPayments: string | undefined,
  // Building fabric & energy certificate
  constructionYear: string | undefined,
  constructionYearByEnergyCertificate: string | undefined,
  constructionPhase: string | undefined,
  furnishing: string | undefined,
  essentialEnergyCarriers: string | undefined,
  energyCertificate: string | undefined,
  energyCertificateType: string | undefined,
  finalEnergyConsumption: string | undefined,
  thinkProtectiveObject: string | undefined,
  modernization: string | undefined,
  heatingType: string | undefined,
  objectState: string | undefined,
  // Furnishing
  furnishingDetail: string | undefined,
  // Miscellaneous
  miscellaneous: string | undefined,
  genericFeatures: string[] | undefined,
  seller: Record<string, unknown> | undefined,
}

export type FullScrapedEntity = BaseScrapedEntity | ScrapedEntityWithAdditionalProperties | {extraPropertiesViaDataObj: Partial<ScrapedEntityWithAdditionalProperties>}

export type FullPageAdditionalProperties = keyof ScrapedEntityWithAdditionalProperties

export type PropertySelectorAndTextParser = {
  selector: string,
  textParser?: (elm: HTMLElement) => string,
  measureParser?: (elm: HTMLElement | string | undefined) => Measurement | undefined,
  removeChildSelector?: string,
  matchText?: string
}

export type FullPageScrapeSelector = {
  [K in FullPageAdditionalProperties]?: PropertySelectorAndTextParser[]
}