import { Source } from "./Source";

export default interface IRuntimeMessageRequest {
  message: string;
  data: {
    [key: string]: any;
  };
  isAsync: boolean;
  source: Source
}
