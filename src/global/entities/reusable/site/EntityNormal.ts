import IPageEntity from "../types/IPageEntity";
import {
    FullPageScrapeSelector,
    FullScrapedEntity,
    PropertySelectorAndTextParser,
    ScrapedEntityWithAdditionalProperties
} from "../types/ScrapedEntity";
import Measurement from "../types/Price";
import ImmoScoutParsePriceNodeAsPrice, { ImmoScoutParsePriceGroupAsPrice } from "../../../helpers/ImmoScoutParsePriceNodeAsPrice";
import getIdFromPath from "../../../helpers/getIdFromPath";
import IIndicator from "../types/Indicator/IIndicator";
import {DealType, RealestateType} from "../utils/rotekapsel";
import {extractProperties} from "../../../helpers/extractProperties";

export interface EntityNormalProps {
    entityDiv: HTMLDivElement;
    dealType: DealType;
    realestateType: RealestateType;
}

export default class EntityNormal implements IPageEntity {
    protected props: EntityNormalProps;
    protected linkNodeSelectors: PropertySelectorAndTextParser[] = []
    protected extraPropertiesSelector: FullPageScrapeSelector = {}
    protected priceSelector: string = ""
    // @ts-ignore
    protected _indicator: IIndicator | undefined;

    constructor(props: EntityNormalProps) {
        this.props = {...props};
    }

    set indicator(indicator: IIndicator | undefined) {
        this._indicator = indicator;
    }

    get indicatorPlacement(): HTMLDivElement | null {
        return this.props.entityDiv;
    }
    // Sample data obj https://jsoneditoronline.org/#left=cloud.add3cc543b50477586939d5df29a9f0c

    adjustExtraProperties(currentResult: ScrapedEntityWithAdditionalProperties): ScrapedEntityWithAdditionalProperties {
        return currentResult;
    }
    get extraProperties(): ScrapedEntityWithAdditionalProperties {
        const clonedDiv = this.props.entityDiv.cloneNode(true) as HTMLElement;
        let result = extractProperties(clonedDiv, this.extraPropertiesSelector);
        result = this.adjustExtraProperties(result)
        return result
    }

    scrapeEntityHelper(): FullScrapedEntity | undefined {
        const linkNode = this.linkNode;
        const extraProperties = this.extraProperties
        if (linkNode) {
            return {
                ...extraProperties,
                guid: this.guid as string,
                path: ((linkNode as any) as HTMLAnchorElement).pathname,
                prices: this.prices,
                dealType: this.props.dealType,
                realestateType: this.props.realestateType,
                isFullEntity: false,
                indicator: this.indicator,
            };
        }
    }
    // eslint-disable-next-line getter-return
    get scrapedEntity(): FullScrapedEntity | undefined {
        return this.scrapeEntityHelper()
    }

    get indicator(): IIndicator | undefined {
        return this._indicator;
    }

    // eslint-disable-next-line getter-return
    get guid(): string | undefined {
        const linkNode = this.linkNode;
        if (linkNode) {
            return getIdFromPath(((linkNode as any) as HTMLAnchorElement).pathname);
        }
    }

    get pricesNodes(): HTMLDivElement[] {
        let maybePriceNodes: NodeListOf<HTMLDivElement>;

        try {
            maybePriceNodes = this.props.entityDiv.querySelectorAll(
                '.listing-details .grid .grid-item .grid-item:first-child span'
            );
            if (maybePriceNodes?.length > 0) {
                return Array.from(maybePriceNodes).reduce((acc: HTMLDivElement[], elm: HTMLDivElement) => {
                    if(elm.innerText.indexOf('€') > -1){
                        acc.push(elm);
                    }
                    return acc;
                }, []);
            } else {
                maybePriceNodes = this.props.entityDiv.querySelectorAll(
                    this.priceSelector
                );
                // @ts-ignore
                return Array.from(maybePriceNodes).reduce((acc: HTMLDivElement[], elm: HTMLDivElement) => {
                    if(elm.innerText.indexOf('€') > -1){
                        acc.push(elm);
                    }
                    return acc;
                }, []);
            }
        } catch (_) {
            return [];
        }
    }

    get linkNode(): HTMLAnchorElement | null {
        for (let sel of this.linkNodeSelectors)
            if (this.props.entityDiv.querySelector(sel.selector))
                return this.props.entityDiv.querySelector(sel.selector)

        return null;
    }

    get prices(): Measurement[] {
        const checkDict: Record<string, boolean> = {};
        return this.pricesNodes.reduce((acc: Measurement[],elm: HTMLDivElement) => {
            const price = elm.tagName === 'DL'
                ? ImmoScoutParsePriceNodeAsPrice(elm, " ")
                : ImmoScoutParsePriceGroupAsPrice(elm, " ");
            if(price){
                // use map to find if already exists and decide if to push
                const key = `${price.number}:${price.measure}:${price.label}`;
                if (!checkDict[key]) {
                    acc.push(price);
                    checkDict[key] = true;
                }
            }
            return acc;
        }, []);
    }
}
