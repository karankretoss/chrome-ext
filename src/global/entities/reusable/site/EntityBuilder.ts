import IPageEntity from "../types/IPageEntity";
import IndicatorNormal from "../types/Indicator/IndicatorNormal";
import IScrapeBuilderOptions from "../types/IScrapeBuilderOptions";
import type {DealType, RealestateType} from "../utils/rotekapsel";
import {dealTypeMap, realestateMap} from "../utils/rotekapsel";
import EntityNormal from "./EntityNormal";

export interface handleListPageParams {
    entityDOMNode: HTMLDivElement,
    dealType: DealType,
    realestateType: RealestateType,
    isProjectAds?: Boolean,
}
export default class EntityBuilder {
    protected entityNormal: typeof EntityNormal = EntityNormal;
    protected shouldShowIndicator: Boolean = true;

    getDealAndRealestateTypesFromText(text: string): [ DealType, RealestateType ]  {
        const [ rt, dt ] = text.split(' ', 2);
        if ('Eigentumswohnung' === rt) {
            return [ 'buy', 'flat'];
        } else if ('Neubauprojekte' === rt) {
            return [ 'buy', 'property'];
        }
        const deDtlower = dt?.toLowerCase() || "";
        const deRtlower = rt?.toLowerCase() || "";

        return [ dealTypeMap[deDtlower], realestateMap[deRtlower]];
    }

    getDealAndRealestateTypesEmptyHandler(dealType: DealType, realestateType: RealestateType): [ DealType, RealestateType] {
        return [dealType, realestateType]
    }

    getDealAndRealestateTypes(dealType?: DealType, realestateType?: RealestateType): [ DealType, RealestateType ]  {
        if (dealType && realestateType) {
            return [ dealType, realestateType ];
        }
        [ dealType, realestateType ] = this.getDealAndRealestateTypesFromText(document.title);
        [dealType, realestateType] = this.getDealAndRealestateTypesEmptyHandler(dealType, realestateType)

        dealType = dealType || 'buy';
        realestateType = realestateType || 'property';
        return [ dealType, realestateType ];
    }

    handleListPage(params: handleListPageParams): IPageEntity[] {
        const {entityDOMNode, dealType, realestateType} = params;
        try {
            const entity = new this.entityNormal({
                entityDiv: entityDOMNode as HTMLDivElement,
                dealType,
                realestateType,
            })
            const indicator = new IndicatorNormal(
                entity.indicatorPlacement as HTMLDivElement,
                this.shouldShowIndicator? {} : {display: 'none'}
            );
            entity.indicator = indicator;
            return [entity];
        } catch (error: any) {
            console.error(`Could not create ImmoScoutPageEntityNormal due to '${error.message}'`);
            return [];
        }
    }

    build(options: IScrapeBuilderOptions): IPageEntity[] {
        const { entityDOMNode, dealType: maybeDealType, realestateType: maybeRealestateType } = options;
        if (!entityDOMNode) {
            return [];
        }

        const [ dealType, realestateType ] = this.getDealAndRealestateTypes(maybeDealType, maybeRealestateType);

        return this.handleListPage({entityDOMNode, dealType, realestateType});
    }
}
