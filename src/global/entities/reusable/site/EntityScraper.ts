import IPageEntity from "../types/IPageEntity";
import IHistoricData from "../types/IHistoricData";
import {waitForPageReady} from "../../../helpers/waitForPageReady";
import BaseScrapedEntity from "../types/ScrapedEntity";
import {InitializeListPageProps} from "../types/misc";
import {ExtensionCustomEvent} from "../types/ExtensionCustomEvent";
import { MAX_RETRY_GET_COUNTRY_AT_EXPOSE_PAGE } from "../../../constants";
import EntityBuilder from "./EntityBuilder";
import Favorite from "../types/Favorite";
import { DealType, RealestateType } from "../../rotekapsel";

declare global {
    interface Window {
        adsData: any
    }
}

export interface EntityScraperProps {
    Builder: typeof EntityBuilder,
    entitiesDOMNodesSelector: string,
    exposeDOMNodesSelector?: string,
    dealType?: DealType;
    realestateType?: RealestateType;
    readinessConditionFunction: () => Boolean,
}

export interface IHighScraperProps {
    dealType?: DealType;
    realestateType?: RealestateType;
}

export default class EntityScraper {
    protected props: EntityScraperProps
    protected builder: EntityBuilder;
    protected entities: IPageEntity[] = []

    constructor(props: EntityScraperProps) {
        this.props = props;
        this.builder = new props.Builder();
    }

    async getInPageJSONData() {
        document.dispatchEvent(new CustomEvent(ExtensionCustomEvent.NotifyFromContentToMainWorldForAdsData));
        return new Promise((resolve, reject) => {
            document.addEventListener(ExtensionCustomEvent.SendAdsDataFromMainWorldCToContent,  (e: any) => {
                if (e.detail && e.detail.payload !== undefined) {
                    const {adsData, counter} = e.detail.payload
                    try {
                        window.adsData = JSON.parse(adsData)
                    } catch (_) {}

                    if (adsData || counter === MAX_RETRY_GET_COUNTRY_AT_EXPOSE_PAGE) {
                        document.dispatchEvent(new CustomEvent(ExtensionCustomEvent.ReceiveAdsDataFromMainWorldCToContent))
                        resolve(true)
                    }
                }
            })
        })
    }
    
    initializeListPageHelper(props: InitializeListPageProps) {
        const { entitiesDOMNodes, isProjectAds } = props;
        const { dealType, realestateType } = this.props;
        return Array.from(entitiesDOMNodes)
            .map((entityDOMNode: HTMLDivElement) =>
                this.builder.build({ entityDOMNode, isProjectAds, dealType, realestateType })
            )
            .reduce((acc: IPageEntity[], entities: IPageEntity[]) => {
                entities.forEach((entity) => {
                    if (entity?.guid) {
                        acc.push(entity)
                    }
                });
                return acc;
            }, [] as IPageEntity[]) || []
    }

    async initializeListPage(): Promise<void> {
        await waitForPageReady(this.props.readinessConditionFunction);
        await this.getInPageJSONData();
        const entitiesDOMNodes: NodeListOf<HTMLDivElement> = document.querySelectorAll(this.props.entitiesDOMNodesSelector || "") || [];
        this.entities =  this.initializeListPageHelper({entitiesDOMNodes})
    }

    async initializeExposePage(): Promise<void> {
        if (!this.props.exposeDOMNodesSelector) {
            return;
        }
        await this.getInPageJSONData()
        const entityDOMNode = document.querySelector(this.props.exposeDOMNodesSelector) as HTMLDivElement;
        this.entities = this.builder.build({ entityDOMNode });
    }
    async getPageEntities(): Promise<BaseScrapedEntity[]> {
        this.entities = this.entities || [];
        return this.entities
            .map((entity) => {
                if (!entity.scrapedEntity) {
                  return undefined;
                }
                const {extraPropertiesViaDataObj, ...rest} = entity.scrapedEntity as any
                return rest;
            })
            .filter((scrapedEntity: any) => !!scrapedEntity) as any;
    }

    async getFavorites(): Promise<Favorite[]> {
        return []
    }
    updateIndicators(historicData: IHistoricData[]): void {
        if (!(historicData && historicData.length)) {
            return;
        }
        historicData.forEach((history: IHistoricData) => {
            const entity = this.entities?.find(
                (entity) => entity.guid === history.guid
            );
            if (entity && entity.indicator) {
                entity.indicator.updateHistory(history);
            }
        });
    }
}
