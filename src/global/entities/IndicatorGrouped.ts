import IIndicator from "./IIndicator";
import SPINNER_SVG from "../UI/svg/spinner";
import IndicatorStatus from "./IndicatorStatus";
import IHistoricData from "./IHistoricData";
import TRENDING_FLAT_SVG from "../UI/svg/trendingFlat";
import TRENDING_DOWN_SVG from "../UI/svg/trendingDown";
import TRENDING_UP_SVG from "../UI/svg/trendingUp";
import putMoreInfoPopover from "../UI/putMoreInfoPopover";

const DAY_IN_MS = 1000 * 60 * 60 * 24;
export default class IndicatorNormal implements IIndicator {
  private _element: HTMLDivElement;
  // @ts-ignore
  private history: IHistoricData;

  constructor(
    positionElement: HTMLDivElement,
    styleOverride?: { [key: string]: string }
  ) {
    this._element = document.createElement("div");
    this._element.style.zIndex = "50";
    this._element.style.pointerEvents = "all";
    this._element.style.position = "absolute";
    this._element.style.display = "flex";
    this._element.style.cursor = "pointer";
    this._element.style.backgroundColor = "rgba(256,256,256,0.7)";
    this._element.style.borderRadius = "50px";
    this._element.innerHTML = SPINNER_SVG;

    if (styleOverride) {
      Object.entries(styleOverride).map(([key, value]) => {
        this._element.style[key as any] = value;
      });
    }

    positionElement.append(this._element);
  }

  get element(): HTMLDivElement {
    return this._element;
  }

  get pillText(): string {
    const initialDate =
      this.history.changes.length > 0
        ? new Date(this.history.changes[0].date)
        : new Date();
    const now = new Date();

    const daysPassed = Math.floor(
      (now.getTime() - initialDate.getTime()) / DAY_IN_MS
    );
    return daysPassed === 0
      ? "Neu"
      : daysPassed > 99
      ? "+99 T"
      : daysPassed + " T";
  }

  updateHistory(history: IHistoricData): void {
    this.history = history;

    const priceDiffStatus = this.getPriceDiffStatus(this.history);
    switch (priceDiffStatus) {
      case IndicatorStatus.SAME: {
        this.element.innerHTML = `
        <div style="width: 29px; height: 21px; border-radius: 20px 0px 0px 20px; box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.05); display: flex; justify-content: center; align-items: center; background: linear-gradient(180deg, #A388DD 0%, #8F6ED5 100%);">
          ${TRENDING_FLAT_SVG({ size: 20 })}
        </div>
        <div style="width: 38px; height: 19px; font-size: 11px; background: #FFFFFF; border: 1px solid #E1E5EA; box-sizing: border-box; box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.05); border-radius: 0px 20px 20px 0px; display: flex; justify-content: center; align-items: center;">
          ${this.pillText}
        </div>
        `;
        // this.element.innerHTML = TRENDING_FLAT_SVG;
        break;
      }
      case IndicatorStatus.DOWN: {
        this.element.innerHTML = `
        <div style="width: 29px; height: 21px; border-radius: 20px 0px 0px 20px; box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.05); display: flex; justify-content: center; align-items: center; background: linear-gradient(180deg, #66C5FF 0%, #33B1FF 100%);">
          ${TRENDING_DOWN_SVG({ size: 20 })}
        </div>
        <div style="width: 38px; height: 19px; font-size: 11px; background: #FFFFFF; border: 1px solid #E1E5EA; box-sizing: border-box; box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.05); border-radius: 0px 20px 20px 0px; display: flex; justify-content: center; align-items: center;">
          ${this.pillText}
        </div>
        `;
        break;
      }
      case IndicatorStatus.UP: {
        this.element.innerHTML = `
        <div style="width: 29px; height: 21px; border-radius: 20px 0px 0px 20px; box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.05); display: flex; justify-content: center; align-items: center; background: linear-gradient(180deg, #FF974D 0%, #FF832B 100%);">
          ${TRENDING_UP_SVG({ size: 20 })}
        </div>
        <div style="width: 38px; height: 19px; font-size: 11px; background: #FFFFFF; border: 1px solid #E1E5EA; box-sizing: border-box; box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.05); border-radius: 0px 20px 20px 0px; display: flex; justify-content: center; align-items: center;">
          ${this.pillText}
        </div>
        `;
        break;
      }
    }
    putMoreInfoPopover({
      domElement: this.element,
      historicData: this.history,
    });
  }

  private getPriceDiffStatus(entityHistory: IHistoricData): IndicatorStatus {
    const changesWithActualPrice = entityHistory.changes.filter(
      (change) => change.price.currency !== "?"
    );
    if (changesWithActualPrice.length < 2) {
      return IndicatorStatus.SAME;
    }
    if (
      changesWithActualPrice[changesWithActualPrice.length - 1].price.amount >
      changesWithActualPrice[changesWithActualPrice.length - 2].price.amount
    ) {
      return IndicatorStatus.UP;
    }
    if (
      changesWithActualPrice[changesWithActualPrice.length - 1].price.amount <
      changesWithActualPrice[changesWithActualPrice.length - 2].price.amount
    ) {
      return IndicatorStatus.DOWN;
    }
    return IndicatorStatus.SAME;
  }

  setStatus(indicatorStatus: IndicatorStatus): void {}
}
