import getIdFromPath from "../helpers/getIdFromPath";
import {ImmoScoutParsePriceAsPrice} from "../helpers/ImmoScoutParsePriceNodeAsPrice";
import IPageEntity from "./IPageEntity";
import Measurement from "./Price";
import BaseScrapedEntity from "./ScrapedEntity";
import IIndicator from "./IIndicator";
import type {DealType, RealestateType} from "./rotekapsel";

export default class ImmoScoutPageEntityGrouped implements IPageEntity {
  private entityDiv: HTMLDivElement;
  private dealType: DealType;
  private realestateType: RealestateType;
  // @ts-ignore
  private _indicator: IIndicator;

  constructor(entityDiv: HTMLDivElement, dealType: DealType, realestateType: RealestateType) {
    this.entityDiv = entityDiv;
    this.dealType = dealType;
    this.realestateType = realestateType;
  }

  set indicator(indicator: IIndicator) {
    this._indicator = indicator;
  }

  get indicatorPlacement(): HTMLDivElement | null {
    return this.entityDiv.querySelector(".need-to-fill-it");
  }

  // eslint-disable-next-line getter-return
  get scrapedEntity(): BaseScrapedEntity | undefined {
    const linkNode = this.linkNode;
    if (linkNode) {
      return {
        guid: this.guid as string,
        path: ((linkNode as any) as HTMLAnchorElement).pathname,
        prices: this.prices,
        dealType: this.dealType,
        realestateType: this.realestateType,
        isFullEntity: false,
        indicator: this.indicator,
      };
    }
  }

  get indicator(): IIndicator {
    return this._indicator;
  }

  // eslint-disable-next-line getter-return
  get guid(): string | undefined {
    const linkNode = this.linkNode;
    if (linkNode) {
      return getIdFromPath(((linkNode as any) as HTMLAnchorElement).pathname);
    }
  }

  get pricesNodes(): HTMLDivElement[] {
    const dataSpans = this.entityDiv.querySelectorAll(
      ".grouped-listing__criterion"
    );
    if (dataSpans.length > 0) {
      return [dataSpans[0] as HTMLDivElement];
    }
    return [];
  }

  get prices(): Measurement[] {
    const pricesNodes: HTMLDivElement[] = this.pricesNodes;
    return pricesNodes.reduce((acc: Measurement[], priceNode: HTMLDivElement) => {
      if (priceNode) {
        const price = ImmoScoutParsePriceAsPrice(priceNode.innerText, " ");
        if (price) {
          acc.push(price);
        }
      }
      return acc;
    }, []);
  }

  private get linkNode(): HTMLAnchorElement | null {
    return this.entityDiv.querySelector("a");
  }
}
