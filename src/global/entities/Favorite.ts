export default interface Favorite {
    id: string,
    fields: string[]
}