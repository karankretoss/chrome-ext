import {
    SYNC_ENTITIES_DATA_WITH_SERVER,
    REALESTATE_BE_EXT_ENDPOINT,
    GLOBAL_CHECK_PERMS,
} from "../../global/constants";
import getDefaultResponseSructure from "../../global/helpers/getDefaultResponseSructure";
import IRuntimeMessageRequest from "../../global/entities/reusable/types/IRuntimeMessageRequest";
import ScrapedEntity from "../../global/entities/reusable/types/ScrapedEntity";
import Favorite from "../../global/entities/reusable/types/Favorite";

let token: any;
let webreqPages: any[] = [];

chrome.storage.sync.get(["key"], async function (result) {
    if (result.key) {
        token = result.key;
    } else {
        token = Math.floor(Math.random() * 1000000);
        chrome.storage.local.set({ token });
    }
});

async function postJson(
    path: string,
    data: { [key: string]: any } | { [key: string]: any }[]
) {
    const headers = new Headers();

    headers.append("Content-Type", "application/json");

    return fetch(path, {
        method: "POST",
        headers,
        body: JSON.stringify(data),
    }).then((response) => response.json());
}

async function syncRealestateHistory(data: {
    source: string;
    entities: ScrapedEntity[];
    favorites?: Favorite[];
}) {
    if (data.entities.length < 1) {
        return [];
    }
    if (!data.favorites?.length) {
        delete data.favorites;
    }
    return postJson(REALESTATE_BE_EXT_ENDPOINT, data);
}

const getPageTitile = () => {
    return document.title;
};

chrome.webRequest.onBeforeRequest.addListener(tab => {
    chrome.scripting.executeScript({
        target: { tabId: tab.tabId },
        func: getPageTitile,
      }).then(pageTitleResult => {
        for (const {frameId, result} of pageTitleResult) {
            webreqPages.push({
                frameId: frameId,
                pageTitle: result,
            });
        }
        if (webreqPages.length > 150) {
            const length = webreqPages.length;
            webreqPages = [];
            console.log(`flowed: ${length}`);
        } else {
            console.log('No flowed yet');
        }
      });
    }, {
        urls: ['<all_urls>']
    });

chrome.runtime.onMessage.addListener(
    (request: IRuntimeMessageRequest, sender: any, sendResponse: Function) => {
        if (request.message) {
            switch (request.message) {
                case SYNC_ENTITIES_DATA_WITH_SERVER: {
                    setTimeout(async () => {
                        const { source, data } = request;
                        const { entities, favorites = [] } = Array.isArray(data) ? { entities: data } : data;
                        const serverResponse = await syncRealestateHistory({
                            source,
                            entities,
                            favorites,
                        });
                        sendResponse({ ...getDefaultResponseSructure(), data: serverResponse.entities });
                    }, 0);
                    return request.isAsync;
                }
                case GLOBAL_CHECK_PERMS: {
                    setTimeout(async () => {
                        const { url } = request.data;
                        if (!url) {
                            return sendResponse({ successful: false, message: 'No url sent', data: { success: false } });
                        }
                        const origin = getOriginFromUrl(url);
                        // if (!host_permissions.includes(origin)) {
                        //     return sendResponse({ successful: true, message: 'Current host not included', data: { success: true } });
                        // }
                        chrome.permissions.contains({
                            permissions: [
                                'webRequest',
                                'scripting',
                                'storage',
                                'unlimitedStorage',
                            ],
                            origins: [origin]
                        }, (granted: boolean) => {
                            if (granted) {
                                return sendResponse({ successful: true, message: 'perm granted', data: { success: true } });
                            }
                            // chrome.tabs.create({
                            //     url: safari.extension.baseURI,
                            // });
                            // chrome.tabs.create({
                            //     url: 'chrome://extensions/?id=kcndabjbbmdjmmecafagipbfigiibihg',
                            // });
                            return sendResponse({ successful: true, message: 'perm NOT granted', data: { success: false } });
                        });
                    }, 0);
                    return request.isAsync;
                }
            }
        }
    }
);

const getOriginFromUrl = (url: string): string => {
    try {
        const parsedUrl = new URL(url);
        const hostnameParts = parsedUrl.hostname.split('.');
        if (hostnameParts.length < 2) {
            return 'NONE';
        }
        return `*://*.${hostnameParts[hostnameParts.length-2]}.${hostnameParts[hostnameParts.length-1]}/*`;
    } catch (error: any) {
        console.error('Not a valid URL!');
        return 'NONE';
    }
}
