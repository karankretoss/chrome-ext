import onEachPathChange from "../../global/UI/onEachPathChange";
import IRuntimeMessageRequest from "../../global/entities/IRuntimeMessageRequest";
import { GLOBAL_CHECK_PERMS } from "../../global/constants";
import {IResponse} from "../../global/helpers/getDefaultResponseSructure";

const initPage = async () => {
  const url = window.location.href;
  const message: IRuntimeMessageRequest = {
    message: GLOBAL_CHECK_PERMS,
    data: {
      url,
    },
    isAsync: true,
  };
  chrome.runtime.sendMessage(message, (response: IResponse) => {
    if (response?.successful === false || (response?.data as { [key: string]: any })?.success === false) {
      console.log({ response });
    }
  });
};

initPage();

onEachPathChange(initPage);
