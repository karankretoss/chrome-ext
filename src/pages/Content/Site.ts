import onEachPathChange from "../../global/UI/onEachPathChange";
import ImmoScout24EntityScraper from "../../global/entities/sites/immoScout24/immoScout24De/ImmoScout24EntityScraper";
import ImmoScout24EntityScraperAt
    from "../../global/entities/sites/immoScout24/immoScount24At/ImmoScout24EntityScraper";
import WillHabenEntityScraper from "../../global/entities/sites/willhaben/WillHabenEntityScraper";
import KleinanzeigenEntityScraper from "../../global/entities/sites/kleinanzeigen/KleinanzeigenEntityScraper";
import IRuntimeMessageRequest from "../../global/entities/reusable/types/IRuntimeMessageRequest";
import {SYNC_ENTITIES_DATA_WITH_SERVER} from "../../global/constants";
import {IResponse} from "../../global/helpers/getDefaultResponseSructure";
import IHistoricData from "../../global/entities/reusable/types/IHistoricData";
import {deleteAllExistingInstancesOfPopper} from "../../global/UI/popover";
import BaseScrapedEntity from "../../global/entities/reusable/types/ScrapedEntity";
import Favorite from "../../global/entities/reusable/types/Favorite";
import {debounce} from "../../global/entities/reusable/utils/misc";
import { DealType, RealestateType } from "../../global/entities/rotekapsel";

const entitiesSentToServer: BaseScrapedEntity[] = [];

export type Scraper = typeof ImmoScout24EntityScraper | typeof ImmoScout24EntityScraperAt | typeof WillHabenEntityScraper | typeof KleinanzeigenEntityScraper;
export interface notifyServerAndDisplayIndicatorParams {
    entities: BaseScrapedEntity[],
    favorites?: Favorite[]
}

export interface InitPageParams {
    scraper: Scraper,
    dealType?: DealType;
    realestateType?: RealestateType;
    shouldHandleListPage: () => Boolean,
    shouldHandleExposePage: () => Boolean
}

export const notifyServerAndDisplayIndicator = async (data: notifyServerAndDisplayIndicatorParams, scraper: any) => {
    try {
        const {entities, favorites} = data;

        const message: IRuntimeMessageRequest = {
            message: SYNC_ENTITIES_DATA_WITH_SERVER,
            data: {
                favorites,
                entities: entities.map((entity) => {
                    const {indicator, ...entityToSendToServer} = entity;
                    return entityToSendToServer
                })
            },
            source: scraper.source,
            isAsync: true,
        };
        await chrome.runtime.sendMessage(message, (response: IResponse) => {
            if (response.successful) {
                scraper.updateIndicators(response.data as IHistoricData[]);
                entitiesSentToServer.push(...data.entities);
            }
        });
    } catch (_) {}

}

const initPage = async (params: InitPageParams) => {
    const {
        scraper,
        shouldHandleListPage,
        shouldHandleExposePage,
        dealType,
        realestateType
    } = params;

    deleteAllExistingInstancesOfPopper();
    const scraperInstance = new scraper({ dealType, realestateType });

    if (shouldHandleListPage()) {
        await scraperInstance.initializeListPage();
    } else if (shouldHandleExposePage()) {
        await scraperInstance.initializeExposePage();
    }

    const crawledEntities = await scraperInstance.getPageEntities();
    const favorites = await scraperInstance.getFavorites();
    const entitiesToBeSendToServer = crawledEntities.filter(entity => !entitiesSentToServer.find(entitySendToServer => entitySendToServer.guid === entity.guid));
    if (entitiesToBeSendToServer.length) {
        await notifyServerAndDisplayIndicator({entities: entitiesToBeSendToServer, favorites}, scraperInstance)
    }
};

export const main = async (initPageParams: InitPageParams) => {
    try {
        onEachPathChange(() => {
            if (entitiesSentToServer.length) {
                entitiesSentToServer.splice(0, entitiesSentToServer.length);
            }
            initPage(initPageParams);
        });
        window.addEventListener('scroll', debounce( () => initPage(initPageParams), 1000))
        await initPage(initPageParams)
    } catch (_) {
        console.error(_)
    }
}

