import onEachPathChange from "../../../global/UI/onEachPathChange";
import ImmoNetEntityScraper from "../../../global/entities/ImmoNetEntityScraper";
import IRuntimeMessageRequest from "../../../global/entities/reusable/types/IRuntimeMessageRequest";
import {SYNC_ENTITIES_DATA_WITH_SERVER} from "../../../global/constants";
import {IResponse} from "../../../global/helpers/getDefaultResponseSructure";
import IHistoricData from "../../../global/entities/reusable/types/IHistoricData";
import {Source} from "../../../global/entities/reusable/types/Source";

const initPage = async () => {
  const results = document.querySelector("#result-list-stage");
  const isInSpecificResultPage = window.location.href.indexOf("/angebot/") > 0;

  if (results || isInSpecificResultPage) {
    const entityScraper = new ImmoNetEntityScraper();

    if (results) {
      await entityScraper.initializeListPage();
    } else if (window.location.href.indexOf("/angebot/") > 0) {
      await entityScraper.initializeExposePage();
    }

    const crawledEntities = await entityScraper.getPageEntities();
    const message: IRuntimeMessageRequest = {
      message: SYNC_ENTITIES_DATA_WITH_SERVER,
      data: crawledEntities.map(entity => {
        const {indicator, ...rest} = entity;
        return rest;
      }),
      isAsync: true,
      source: Source.immonet
    };
    chrome.runtime.sendMessage(message, (response: IResponse) => {
      if (response.successful) {
        entityScraper.updateIndicators(response.data as IHistoricData[]);
      }
    });
  }
};

initPage();

onEachPathChange(initPage);
