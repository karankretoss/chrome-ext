import ImmoScout24EntityScraper
  from "../../../../global/entities/sites/immoScout24/immoScout24De/ImmoScout24EntityScraper";
import {main} from "../../Site";

const shouldHandleListPage = () => window.location.href.indexOf("/Suche/") > 0 ||
    window.location.href.indexOf("/gewerbe-flaechen/") > 0
const shouldHandleExposePage = () => window.location.href.indexOf("/expose/") > 0;

// const getPageTitile = (userBackgroundColor: string) => {
//   document.body.style.backgroundColor = userBackgroundColor;
//   return document.title;
// };
// const getUserColor = () => { return document.body.style.backgroundColor };
// args: [ getUserColor() ],

(async () => {
  try {
    await main({
      scraper: ImmoScout24EntityScraper,
      shouldHandleExposePage,
      shouldHandleListPage
    })
  } catch (_) {}
})()
