import ImmoScout24EntityScraper
    from "../../../../global/entities/sites/immoScout24/immoScount24At/ImmoScout24EntityScraper";
import {main} from "../../Site";

const shouldHandleListPage = () => window.location.href.indexOf("/Suche/") > 0 ||
    window.location.href.indexOf("/regional/") > 0
const shouldHandleExposePage = () => false;

(async () => {
    try {
        await main({
            scraper: ImmoScout24EntityScraper,
            shouldHandleExposePage,
            shouldHandleListPage
        })
    } catch (_) {}
})()
