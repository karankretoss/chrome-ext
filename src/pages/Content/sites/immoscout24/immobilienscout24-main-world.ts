import {ExtensionCustomEvent} from "../../../../global/entities/reusable/types/ExtensionCustomEvent";
import {INTERVAL_GET_COUNTRY_AT_EXPOSE_PAGE, MAX_RETRY_GET_COUNTRY_AT_EXPOSE_PAGE} from "../../../../global/constants";

function removeEmptyAndFunctions(obj: any): any {
    if (typeof obj !== 'object' || obj === null) {
        return obj;
    }

    if (Array.isArray(obj)) {
        // Remove functions and empty values from the array
        return obj
            .map((item) => removeEmptyAndFunctions(item))
            .filter((item) => !(typeof item === 'function' || item === null || item === undefined));
    }

    const result: any = {};

    for (const [key, value] of Object.entries(obj)) {
        if (!(typeof value === 'function' || value === null || value === undefined)) {
            if (typeof value === 'object') {
                // Recursively remove functions and empty values from nested objects
                result[key] = removeEmptyAndFunctions(value);
            } else {
                result[key] = value;
            }
        }
    }

    return result;
}
const getIS24Wrapper = () => {
    try {
        const result = JSON.stringify(removeEmptyAndFunctions(window.IS24))
        if (result === '{}') {
            return undefined
        }
        return result
    } catch (_) {
        return undefined
    }
}

const main = () => {
    let dataReceivedAtIsolatedContentScript = false
    let counter = 0

    const t = setInterval(function () {
        counter += 1;
        if (counter > MAX_RETRY_GET_COUNTRY_AT_EXPOSE_PAGE || dataReceivedAtIsolatedContentScript) {
            clearInterval(t)
            return;
        }
        document.dispatchEvent(new CustomEvent(ExtensionCustomEvent.SendAdsDataFromMainWorldCToContent, {
            detail: {
                payload: {
                    adsData: getIS24Wrapper(),
                    counter
                }
            }
        }));
    }, INTERVAL_GET_COUNTRY_AT_EXPOSE_PAGE)

    document.addEventListener(ExtensionCustomEvent.ReceiveAdsDataFromMainWorldCToContent, async function (e: any) {
        dataReceivedAtIsolatedContentScript = true;
    })
}

document.addEventListener(ExtensionCustomEvent.NotifyFromContentToMainWorldForAdsData, async function(e) {
    main();
})