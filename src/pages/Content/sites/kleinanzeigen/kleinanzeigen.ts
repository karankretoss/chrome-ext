import { DealType, RealestateType } from "../../../../global/entities/rotekapsel";
import KleinanzeigenEntityScraper from "../../../../global/entities/sites/kleinanzeigen/KleinanzeigenEntityScraper";
import {main} from "../../Site";

const pageListUrls = [
    's-immobilien',
    's-auf-zeit-wg',
    's-wohnung-kaufen',
    's-ferienwohnung-ferienhaus',
    's-garage-lagerraum',
    's-gewerbeimmobilien',
    's-grundstuecke-garten',
    's-haus-kaufen',
    's-haus-mieten',
    's-wohnung-mieten',
];
const pathFirst = (window.location.href.split('/') ?? [])[3] ?? '(())';

const shouldHandleListPage = () => pageListUrls.includes(pathFirst);
const shouldHandleExposePage = () => false;

const basicPropsMapper: { [key: string]: { dealType: DealType, realestateType: RealestateType } } = {
    's-immobilien': { dealType: 'unknown', realestateType: 'property' },
    's-auf-zeit-wg': { dealType: 'rent', realestateType: 'property' },
    's-wohnung-kaufen': { dealType: 'buy', realestateType: 'flat' },
    's-ferienwohnung-ferienhaus': { dealType: 'unknown', realestateType: 'property' },
    's-garage-lagerraum': { dealType: 'unknown', realestateType: 'parking' },
    's-gewerbeimmobilien': { dealType: 'unknown', realestateType: 'property' },
    's-grundstuecke-garten': { dealType: 'unknown', realestateType: 'property' },
    's-haus-kaufen': { dealType: 'buy', realestateType: 'house' },
    's-haus-mieten': { dealType: 'rent', realestateType: 'house' },
    's-wohnung-mieten': { dealType: 'rent', realestateType: 'flat' },
};

(async () => {
    try {
        await main({
            scraper: KleinanzeigenEntityScraper,
            dealType: basicPropsMapper[pathFirst]['dealType'],
            realestateType: basicPropsMapper[pathFirst]['realestateType'],
            shouldHandleExposePage,
            shouldHandleListPage
        })
    } catch (_) {}
})();

