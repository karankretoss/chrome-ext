import WillHabenEntityScraper from "../../../../global/entities/sites/willhaben/WillHabenEntityScraper";
import {main} from "../../Site";

// const pageListUrls = [
//     's-immobilien',
//     // 's-auf-zeit-wg',
//     's-wohnung-kaufen',
//     // 's-ferienwohnung-ferienhaus',
//     's-garage-lagerraum',
//     // 's-gewerbeimmobilien',
//     // 's-grundstuecke-garten',
//     's-haus-kaufen',
//     's-haus-mieten',
//     's-wohnung-mieten',
// ];
// const pathFirst = (window.location.href.split('/') ?? [])[3] ?? '(())';

const shouldHandleListPage = () => window.location.href.indexOf("/immobilien/") > 0 && window.location.href.indexOf('angebote') > 0
const shouldHandleExposePage = () => false;

(async () => {
    try {
        await main({
            scraper: WillHabenEntityScraper,
            shouldHandleExposePage,
            shouldHandleListPage
        })
    } catch (_) {}
})()
