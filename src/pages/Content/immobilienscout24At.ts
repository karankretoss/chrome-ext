// we don't want to bother users with unnecessary permission popups
// so this script is used as a future placeholder
import onEachPathChange from "../../global/UI/onEachPathChange";

const initPage = async (): Promise<boolean> => {
    return true;
};

initPage();

onEachPathChange(initPage);
