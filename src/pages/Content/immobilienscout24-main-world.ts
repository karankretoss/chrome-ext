import {ExtensionCustomEvent} from "../../global/entities/ExtensionCustomEvent";
import {INTERVAL_GET_COUNTRY_AT_EXPOSE_PAGE, MAX_RETRY_GET_COUNTRY_AT_EXPOSE_PAGE} from "../../global/constants";

function removeEmptyAndFunctions(obj: any): any {
    if (typeof obj !== 'object' || obj === null) {
        return obj;
    }

    if (Array.isArray(obj)) {
        return obj.map(removeEmptyAndFunctions);
    }

    return Object.entries(obj).reduce((acc, [key, value]) => {
        if (typeof value !== 'function' && value !== undefined && value !== null) {
            if (typeof value === 'object') {
                const cleanedValue = removeEmptyAndFunctions(value);
                if (Object.keys(cleanedValue).length > 0) {
                    acc[key] = cleanedValue;
                }
            } else {
                acc[key] = value;
            }
        }
        return acc;
    }, {} as any);
}

const getIS24Wrapper = () => {
    try {
        const result = JSON.stringify(removeEmptyAndFunctions(window.IS24))
        if (result === '{}') {
            return undefined
        }
        return result
    } catch (_) {
        return undefined
    }
}

const main = () => {
    let dataReceivedAtIsolatedContentScript = false
    let counter = 0

    const t = setInterval(function () {
        counter += 1;
        if (counter > MAX_RETRY_GET_COUNTRY_AT_EXPOSE_PAGE || dataReceivedAtIsolatedContentScript) {
            clearInterval(t)
            return;
        }
        document.dispatchEvent(new CustomEvent(ExtensionCustomEvent.SendIS24FromMainWorldCToContent, {
            detail: {
                payload: {
                    IS24: getIS24Wrapper(),
                    counter
                }
            }
        }));
    }, INTERVAL_GET_COUNTRY_AT_EXPOSE_PAGE)

    document.addEventListener(ExtensionCustomEvent.ReceiveS24FromMainWorldCToContent, async function (e: any) {
        dataReceivedAtIsolatedContentScript = true;
    })
}

document.addEventListener(ExtensionCustomEvent.NotifyFromContentToMainWorldForIS24, async function(e) {
    main();
})