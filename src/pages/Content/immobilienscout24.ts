import onEachPathChange from "../../global/UI/onEachPathChange";
import ImmoScout24EntityScraper from "../../global/entities/ImmoScout24EntityScraper";
import IRuntimeMessageRequest from "../../global/entities/IRuntimeMessageRequest";
import {SYNC_ENTITIES_DATA_WITH_SERVER} from "../../global/constants";
import {IResponse} from "../../global/helpers/getDefaultResponseSructure";
import IHistoricData from "../../global/entities/IHistoricData";
import {deleteAllExistingInstancesOfPopper} from "../../global/UI/popover";


const initPage = async () => {
  deleteAllExistingInstancesOfPopper();
  const entityScraper = new ImmoScout24EntityScraper();

  if (
    window.location.href.indexOf("/Suche/") > 0 ||
    window.location.href.indexOf("/gewerbe-flaechen/") > 0
  ) {
    await entityScraper.initializeListPage();
  } else if (window.location.href.indexOf("/expose/") > 0) {
    await entityScraper.initializeExposePage();
  }

  const crawledEntities = await entityScraper.getPageEntities();
  const favorites = await entityScraper.getFavorites();
  const message: IRuntimeMessageRequest = {
    message: SYNC_ENTITIES_DATA_WITH_SERVER,
    data: {
      entities: crawledEntities.map(entity => {
        const {indicator, ...rest} = entity;
        return rest;
      }),
      favorites
    },
    isAsync: true,
  };
  chrome.runtime.sendMessage(message, (response: IResponse) => {
    if (response.successful) {
      entityScraper.updateIndicators(response.data as IHistoricData[]);
    }
  });
};

initPage();

onEachPathChange(initPage);
