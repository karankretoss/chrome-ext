import onEachPathChange from "../../global/UI/onEachPathChange";
import ImmoWeltEntityScraper from "../../global/entities/ImmoWeltEntityScraper";
import IRuntimeMessageRequest from "../../global/entities/IRuntimeMessageRequest";
import { SYNC_ENTITIES_DATA_WITH_SERVER } from "../../global/constants";
import { IResponse } from "../../global/helpers/getDefaultResponseSructure";
import IHistoricData from "../../global/entities/IHistoricData";

const initPage = async () => {
  const currentLocation = window.location.href;
  let resCount: number;
  const results = document.querySelectorAll(`[class^="SearchList"]`);
  const isInSpecificResultPage = window.location.href.indexOf("/expose/") > 0;

  if (results.length > 0 || isInSpecificResultPage) {
    const entityScraper = new ImmoWeltEntityScraper();

    if (results.length > 0) {
      await entityScraper.initializeListPage();
      resCount = document.querySelectorAll(`[class^="EstateItem"]`).length;

      // in immowelt more result are loading after page scroll of a certainPoint
      // so listen to this point and initPage again when it happens:
      document.addEventListener("scroll", function scrollEventListener(e) {
        const currentResCount = document.querySelectorAll(
          `[class^="EstateItem"]`
        ).length;
        if (currentResCount !== resCount) {
          entityScraper.destroy();
          document.removeEventListener("scroll", scrollEventListener);
          initPage();
        }
      });
    } else if (isInSpecificResultPage) {
      await entityScraper.initializeExposePage();
    }

    const crawledEntities = await entityScraper.getPageEntities();

    const message: IRuntimeMessageRequest = {
      message: SYNC_ENTITIES_DATA_WITH_SERVER,
      data: crawledEntities.map(entity => {
        const {indicator, ...rest} = entity;
        return rest;
      }),
      isAsync: true,
    };
    chrome.runtime.sendMessage(message, (response: IResponse) => {
      if (response.successful) {
        entityScraper.updateIndicators(response.data as IHistoricData[]);
      }
    });
  }
};

initPage();

onEachPathChange(initPage);
