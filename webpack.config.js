const webpack = require('webpack'),
  path = require('path'),
  fileSystem = require('fs-extra'),
  env = require('./utils/env'),
  CopyWebpackPlugin = require('copy-webpack-plugin'),
  TerserPlugin = require('terser-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const ASSET_PATH = process.env.ASSET_PATH || '/';

const alias = {
};

// load the secrets
const secretsPath = path.join(__dirname, 'secrets.' + env.NODE_ENV + '.js');

const fileExtensions = [
  'jpg',
  'jpeg',
  'png',
  'gif',
  'eot',
  'otf',
  // 'svg',
  'ttf',
  'woff',
  'woff2',
];

if (fileSystem.existsSync(secretsPath)) {
  alias['secrets'] = secretsPath;
}

const pages =  path.join(__dirname, 'src', 'pages')
const contentScriptPages = path.join(pages, 'Content', 'sites');

const options = {
  mode: process.env.NODE_ENV || 'development',
  entry: {
    background: path.join(pages, 'Background', 'index.ts'),
    autoscout24: path.join(contentScriptPages, 'autoscout24.ts'),
    bakeca: path.join(contentScriptPages,  'bakeca.ts'),
    casaIt: path.join(contentScriptPages,  'casaIt.ts'),
    engelvoelkers: path.join(contentScriptPages,  'engelvoelkers.ts'),
    flatfox: path.join(contentScriptPages,  'flatfox.ts'),
    fotocasaEs: path.join(contentScriptPages,  'fotocasaEs.ts'),
    habitaclia: path.join(contentScriptPages,  'habitaclia.ts'),
    homegate: path.join(contentScriptPages,  'homegate.ts'),
    idealista: path.join(contentScriptPages,  'idealista.ts'),
    immobiliare: path.join(contentScriptPages,  'immobiliare.ts'),
    immobilienscout24MainWorld: path.join(contentScriptPages, 'immoscout24', 'immobilienscout24-main-world.ts'),
    immobilienscout24: path.join(contentScriptPages, 'immoscout24', 'immobilienscout24.ts'),
    immobilienscout24At: path.join(contentScriptPages, 'immoscout24', 'immobilienscout24At.ts'),
    immogencontent: path.join(contentScriptPages, 'immogencontent.ts'),
    immonet: path.join(contentScriptPages,  'immonet.ts'),
    immoscout24Ch: path.join(contentScriptPages,  'immoscout24Ch.ts'),
    immowelt: path.join(contentScriptPages,  'immowelt.ts'),
    immoweltAt: path.join(contentScriptPages,  'immoweltAt.ts'),
    kleinanzeigen: path.join(contentScriptPages, 'kleinanzeigen', 'kleinanzeigen.ts'),
    kleinanzeigenAt: path.join(contentScriptPages, 'kleinanzeigen', 'kleinanzeigenAt.ts'),
    markt: path.join(contentScriptPages,  'markt.ts'),
    mobile: path.join(contentScriptPages,  'mobile.ts'),
    nestoria: path.join(contentScriptPages,  'nestoria.ts'),
    newhome: path.join(contentScriptPages,  'newhome.ts'),
    pisos: path.join(contentScriptPages,  'pisos.ts'),
    rotekapselDe: path.join(contentScriptPages,  'rotekapselDe.ts'),
    thribee: path.join(contentScriptPages,  'thribee.ts'),
    vonPoll: path.join(contentScriptPages,  'vonPoll.ts'),
    wgGesucht: path.join(contentScriptPages,  'wgGesucht.ts'),
    willhaben: path.join(contentScriptPages, 'willhaben', 'willhaben.ts'),
    yaencontre: path.join(contentScriptPages,  'yaencontre.ts'),
  },
  chromeExtensionBoilerplate: {
    notHotReload: ['background', 'contentScript', 'devtools'],
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'build'),
    clean: true,
    publicPath: ASSET_PATH,
  },
  module: {
    rules: [
      {
        // look for .css or .scss files
        test: /\.(css|scss)$/,
        // in the `src` directory
        use: [
          {
            loader: 'style-loader',
          },
          {
            loader: 'css-loader',
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
            },
          },
        ],
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      },
      {
        test: new RegExp('.(' + fileExtensions.join('|') + ')$'),
        type: 'asset/resource',
        exclude: /node_modules/,
        // loader: 'file-loader',
        // options: {
        //   name: '[name].[ext]',
        // },
      },
      {
        test: /\.html$/,
        loader: 'html-loader',
        exclude: /node_modules/,
      },
      { test: /\.(ts|tsx)$/, loader: 'ts-loader', exclude: /node_modules/ },
      {
        test: /\.(js|jsx)$/,
        use: [
          {
            loader: 'source-map-loader',
          },
          {
            loader: 'babel-loader',
          },
        ],
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    alias: alias,
    extensions: fileExtensions
      .map((extension) => '.' + extension)
      .concat(['.js', '.jsx', '.ts', '.tsx', '.css']),
  },
  plugins: [
    new CleanWebpackPlugin({ verbose: false }),
    new webpack.ProgressPlugin(),
    // expose and write the allowed env vars on the compiled bundle
    new webpack.EnvironmentPlugin(['NODE_ENV']),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: 'src/manifest.json',
          to: path.join(__dirname, 'build'),
          force: true,
          transform: function (content, path) {
            // generates the manifest file using the package.json informations
            return Buffer.from(
              JSON.stringify({
                description: process.env.npm_package_description,
                version: process.env.npm_package_version,
                ...JSON.parse(content.toString()),
              })
            );
          },
        },
      ],
    }),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: 'src/assets/_locales/',
          to: path.join(__dirname, 'build', '_locales'),
          toType: 'dir'
        },
      ],
    }),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: 'src/pages/Content/content.styles.css',
          to: path.join(__dirname, 'build'),
          force: true,
        },
      ],
    }),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: 'src/assets/img/',
          to: path.join(__dirname, 'build'),
          force: true,
        },
      ],
    }),
  ],
  infrastructureLogging: {
    level: 'info',
  },
};

if (env.NODE_ENV === 'development') {
  options.devtool = 'inline-cheap-module-source-map';
} else {
  options.optimization = {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        extractComments: false,
      }),
    ],
  };
}

module.exports = options;
